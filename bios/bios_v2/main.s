  .org $8000

  .include ../lib/lib.s

init:
  jsr disableDraw

  ; Draw black screen while stuff loads
  lda #0
  sta temp
  lda #$ff
  sta tileIndex
  jsr solidTile
  jsr fillMap

  ; Select disk load mode
  jsr getDiskMode
  lda #1
  clc
  cmp temp
  beq .loadMode_1

  ; Mode 0 loads all tiles & full 256x256 map
  .loadMode_0:
    jsr dumpTiles
    jsr loadMap
    jmp .loadProgram

  ; Mode 0 loads all tiles & only first 25x19 map tiles
  .loadMode_1:
    jsr dumpTiles
    jsr loadScreen
    jmp .loadProgram

  ; Load program from disk into RAM & beginning running it
  .loadProgram:
    jsr loadProgram
  
  .include ../lib/basic.s
  .include ../lib/basic_impl.s

  .org $fffc
  .word reset
  .word irq
