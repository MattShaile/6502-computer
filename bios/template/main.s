  .org $8000

  .include ../lib/lib.s

init:
  ; Initialise stuff here
  rts

loop:
  ; Main loop here
  jmp loop

  .org $fffc
  .word reset
  .word $0000
