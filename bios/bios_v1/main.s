  .org $8000

  .include ../lib/lib.s

init:
  jsr disableDraw

  ; Draw black screen while stuff loads
  lda #0
  sta temp
  lda #$ff
  sta tileIndex
  jsr solidTile
  jsr fillMap

  ; Load 256 tiles from disk
  jsr loadTiles

  ; Load map from disk
  jsr loadMap

  ; Load program from disk and execute
  jsr loadProgram

  .org $fffc
  .word reset
  .word irq
