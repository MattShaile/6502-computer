import { C6502 } from "components/cpu/c6502";

export class Clock {
    /**
     * @param frequency In Hz
     * @param cpu 
     * @param method Custom method to call, or cpu.clock() if not specified
     */
    constructor(protected frequency: number, protected cpu: C6502, protected method?: Function) {
        if (!this.method) {
            this.method = () => cpu.clock();
        }
    }

    public start() {
        this.loop();
    }

    protected loop() {
        const startClocks = this.cpu.cycles;

        let clocksPerFrame = (this.frequency / 1000) * 60;

        while (this.cpu.cycles - startClocks < clocksPerFrame) {
            if (this.cpu.ready) {
                this.method();
            } else {
                break;
            }
        }

        requestAnimationFrame(() => this.loop());
    }
}