import { C6502, Flags } from "components/cpu/c6502";
import { RAM } from "components/memory/RAM";
import { C6522 } from "components/via/c6522";
import { Binary } from "util/binary";
import { Hex } from "util/hex";

/**
 * Managed the HTML debug elements
 * @param cpu 
 * @param memory        Array of components which have a reset() method, assumes first will be RAM with stack
 * @param clock         Optional. Function to call each clock, defaults to cpu.clock()
 * @param clear         Optional. Function to call to clear the screen on resets etc
 */
export function SetupDebugControls(cpu: C6502, components: Array<{ reset: () => void }>, clock?: () => void, clear?: () => void) {
    if (!clock) {
        clock = () => cpu.clock();
    }

    // Button controls
    const startButton = (document.getElementById("start") as HTMLButtonElement);
    const stopButton = (document.getElementById("stop") as HTMLButtonElement);
    const stepButton = (document.getElementById("step") as HTMLButtonElement);
    const stepmanyButton = (document.getElementById("stepmany") as HTMLButtonElement);
    const resetButton = (document.getElementById("reset") as HTMLButtonElement);
    const ramDump = (document.getElementById("ramdump") as HTMLButtonElement);

    startButton.onclick = () => {
        cpu.ready = true;
        cpu.debug = false;
    }

    stopButton.onclick = () => {
        cpu.ready = false;
    }

    stepButton.onclick = () => {
        cpu.ready = true;
        cpu.debug = true;
        clock();
        cpu.ready = false;
    }

    stepmanyButton.onclick = () => {
        cpu.ready = true;
        const steps = parseInt(prompt("Enter number of steps:"));
        for (let i = 0; i < steps; i++) {
            clock();
        }
        cpu.ready = false;
    }

    resetButton.onclick = () => {
        cpu.ready = false;

        cpu.reset();
        components.forEach((compopnent) => compopnent.reset());

        if (clear) {
            clear();
        }
    }

    ramDump.onclick = () => {
        console.log("RAM Contents", formatBinary((components[0] as any).contents));
    }

    // Stack cells
    const stackCells: Array<{ address: HTMLTableDataCellElement, data: HTMLTableDataCellElement }> = [];
    const stackTable = document.getElementById("stack");
    for (let i = 0; i < 30; i++) {
        const row = document.createElement("tr");
        const addressCell = document.createElement("td");
        const dataCell = document.createElement("td");
        stackTable.appendChild(row);
        row.appendChild(addressCell);
        row.appendChild(dataCell);
        stackCells.push({ address: addressCell, data: dataCell });

        addressCell.innerText = "10000";
    }

    // UI
    function updateUI() {

        // Update button enableds
        startButton.disabled = cpu.ready;
        stopButton.disabled = !cpu.ready;
        stepButton.disabled = cpu.ready;
        stepmanyButton.disabled = cpu.ready;

        // Update register displays
        document.getElementById("pc").innerText = Hex(cpu.reg.pc, 4);
        document.getElementById("sp").innerText = Hex(cpu.reg.sp, 4);
        document.getElementById("a").innerText = Hex(cpu.reg.a);
        document.getElementById("x").innerText = Hex(cpu.reg.x);
        document.getElementById("y").innerText = Hex(cpu.reg.y);

        document.getElementById("n").innerText = cpu.reg.getFlag(Flags.NEGATIVE).toString();
        document.getElementById("v").innerText = cpu.reg.getFlag(Flags.OVERFLOW).toString();
        document.getElementById("b").innerText = cpu.reg.getFlag(Flags.BRK).toString();
        document.getElementById("d").innerText = cpu.reg.getFlag(Flags.DEC).toString();
        document.getElementById("i").innerText = cpu.reg.getFlag(Flags.IRQB).toString();
        document.getElementById("z").innerText = cpu.reg.getFlag(Flags.ZERO).toString();
        document.getElementById("c").innerText = cpu.reg.getFlag(Flags.CARRY).toString();

        components.forEach((component) => {
            if (component instanceof C6522) {
                document.getElementById("portA").innerText = Binary(component["ioA"] || 0);
                document.getElementById("portB").innerText = Binary(component["ioB"] || 0);
                document.getElementById("ddrA").innerText = Binary(component["ddrA"] || 0);
                document.getElementById("ddrB").innerText = Binary(component["ddrB"] || 0);
            }
        })

        stackCells.forEach((cells, index) => {
            const sp = 0x1ff - index;
            cells.address.innerHTML = Hex(sp, 3);
            if (components[0] instanceof RAM) {
                cells.data.innerHTML = Hex((components[0] as RAM)["contents"][sp]);
            }
            if (cpu.reg.sp === sp) {
                cells.address.className = "selected";
                cells.data.className = "selected";
            } else {
                cells.address.className = "";
                cells.data.className = "";
            }
        });

        requestAnimationFrame(updateUI);
    }

    function formatBinary(bytes: number[]) {
        let formatted = "\n0000: ";
        let chunk: string[] = [];
        let lineChunkCount = 0;
        let lineNumber = 0;
        bytes.forEach((byte) => {
            let hex = byte.toString(16);
            if (hex.length < 2) {
                hex = "0" + hex;
            }
            chunk.push(hex);
            if (chunk.length === 8) {
                formatted += chunk.join(" ");
                chunk = [];

                lineChunkCount++;
                if (lineChunkCount === 1) {
                    formatted += "  ";
                } else if (lineChunkCount === 2) {
                    lineChunkCount = 0;
                    lineNumber += 16;
                    let lineStr = lineNumber.toString(16);
                    while (lineStr.length < 4) {
                        lineStr = "0" + lineStr;
                    }
                    formatted += "\n" + lineStr + ": ";
                }
            }
        });

        return formatted;
    }


    (window as any).updateUI = updateUI;
    updateUI();

}