import { Addressable } from "components/addressable";
import { Signal } from "signals";
import { Hex } from "util/hex";
import { OpcodeDecoder } from "./operations/opcode-decoder";

// TODO: Add overflow handling to address bus and data bus
export class C6502 {
    // Registers
    public reg: Registers;

    // 16 bit address bus
    protected _address: number;

    // 8 bit data bus
    public data: number;

    // Enabled / disabled cpu
    public ready: boolean = true;

    // Enable debug logs
    public debug: boolean = false;
    public cycles: number = 0;

    protected decoder: OpcodeDecoder;

    // Simulating IRQB pin state
    protected interrupted: boolean;
    // Indicates whether an interrupt should be handled next clock cycle
    protected interruptPending: InterruptType = InterruptType.NONE;

    protected devices: Addressable[] = [];

    constructor(protected resetVector: number = 0xfffc, protected startFromResetVector: boolean = false) {
        this.decoder = new OpcodeDecoder();

        this.reg = new Registers(0, 0, 0, resetVector, 0xff, 0b00110000);
        this.reset();
    }

    public attachDevice(device: Addressable) {
        this.devices.push(device);
    }

    public reset() {
        this.cycles = 7;
        this.reg.a = 0;
        this.reg.x = 0;
        this.reg.y = 0;
        if (this.startFromResetVector) {
            // Start executing from resetVector
            this.reg.pc = this.resetVector;
        } else {
            // Read the pc start location from resetVector (like 6502 really would)
            this.address = this.resetVector;
            this.read();
            const resetAddressLower = this.data;
            this.address = this.resetVector + 1;
            this.read();
            const resetAddressUpper = this.data;
            this.reg.pc = resetAddressLower + (resetAddressUpper << 8);
        }
        this.reg.sp = 0xff;
        this.reg.status = 0b00110000;
    }

    // Next clock cycle
    public clock() {

        // Handle interrupts
        this.handleInterrupt();

        // Set the program counter address onto the address bus
        this.address = this.reg.pc;

        // Read from memory
        this.read();

        // Decode opcode
        const opcode = this.data;

        if (opcode === 0x0 || opcode === 0xdb) {
            if (this.ready) {
                console.log("BREAK", opcode);
                console.log("PC:", Hex(this.reg.pc));
                console.log(`Clock count: ${this.cycles} (${Math.round(this.cycles / 10000) / 100} seconds)`);
                this.ready = false;
                this.reg.pc++;
                return;
            }
        }

        const operation = this.decoder.decode(opcode);

        // Execute operation
        const logPC = this.reg.pc;
        const logStatus = `A:${Hex(this.reg.a, 2, "")} X:${Hex(this.reg.x, 2, "")} Y:${Hex(this.reg.y, 2, "")} P:${Hex(this.reg.status, 2, "")} SP:${Hex(this.reg.sp, 2, "")} PPU:  ${Math.floor((this.cycles * 3) / 341)},${(this.cycles * 3) % 341} CYC:${this.cycles}`;
        const log = operation.execute(this);
        this.cycles += log.cycles;

        if (this.debug) {
            const logMessage = `${Hex(logPC, 2, "")}  ${Hex(opcode, 2, "")} ${log.operands}  ${log.opName}                             ${logStatus} >` + this.data;
            console.log(logMessage);
            // setTimeout(console.log.bind(console, logMessage));
        }

        // Move program counter forward
        this.reg.pc++;
    }

    public read() {
        const device = this.devices.find((device) => device.inAddressSpace());
        if (device) {
            device.read();
        }
        return this.data;
    }

    public write() {
        const device = this.devices.find((device) => device.inAddressSpace());
        if (device) {
            device.write();
        }
    }

    // Simulate IRQB going low
    public irq() {
        if (!this.reg.getFlag(Flags.IRQB) && !this.interrupted) {
            this.interruptPending = InterruptType.IRQ;
            this.interrupted = true;
        }
    }

    public nmi() {
        this.interruptPending = InterruptType.NMI;
    }

    // Simulate IRQB going high
    public clearInterrupt() {
        this.interrupted = false;
    }

    protected handleInterrupt() {
        if (this.interruptPending !== InterruptType.NONE) {
            // Store flags now to be restored on RTI
            const storeFlags = this.reg.status;

            // push pc to stack
            const pcLowerByte = this.reg.pc & 0xff;
            const pcUpperByte = this.reg.pc >> 8;

            this.address = this.reg.sp + 0x100;
            this.data = pcUpperByte;
            this.write();
            this.reg.sp--;

            this.address = this.reg.sp + 0x100;
            this.data = pcLowerByte;
            this.write();
            this.reg.sp--;

            // push flags to stack
            this.address = this.reg.sp + 0x100;
            this.data = storeFlags;
            this.write();
            this.reg.sp--;

            if (this.interruptPending === InterruptType.IRQ) {
                // IRQ, jump to $fffe/$ffff
                this.address = 0xfffe;
                this.reg.pc = this.read();
                this.address = 0xffff;
                this.reg.pc += this.read() << 8;

                // Turn off interrupts as per 6502 spec
                this.reg.setFlag(Flags.IRQB, true);
            } else {
                // NMI, jump to $fffa/$fffb
                this.address = 0xfffa;
                this.reg.pc = this.read();
                this.address = 0xfffb;
                this.reg.pc += this.read() << 8;
            }

            this.interruptPending = InterruptType.NONE;
        }
    }

    public set address(value: number) {
        if (value < 0) {
            value += 0x10000;
        }
        this._address = value & 0xffff;
    }

    public get address() {
        return this._address;
    }
}

class Registers {

    private _a: number;
    private _x: number;
    private _y: number;
    private _sp: number;
    private _pc: number;

    constructor(
        // Accumulator
        a: number,
        // X
        x: number,
        // Y
        y: number,
        // Program counter
        pc: number,
        // Stack pointer
        sp: number,
        // Processor status register (flags)
        public status: number
    ) {
        this._a = a;
        this._x = x;
        this._y = y;
        this._sp = sp;
        this._pc = pc;
    }

    public setStatusFromByte(byte: number) {
        this.setFlag(Flags.CARRY, !!(byte & (1 << Flags.CARRY)));
        this.setFlag(Flags.ZERO, !!(byte & (1 << Flags.ZERO)));
        this.setFlag(Flags.IRQB, !!(byte & (1 << Flags.IRQB)));
        this.setFlag(Flags.DEC, !!(byte & (1 << Flags.DEC)));
        this.setFlag(Flags.BRK, !!(byte & (1 << Flags.BRK)));
        this.setFlag(Flags.UNUSED, !!(byte & (1 << Flags.UNUSED)));
        this.setFlag(Flags.OVERFLOW, !!(byte & (1 << Flags.OVERFLOW)));
        this.setFlag(Flags.NEGATIVE, !!(byte & (1 << Flags.NEGATIVE)));
    }

    public set a(value: number) {
        if (value < 0) {
            value += 0x100;
        }
        this._a = value & 0xff;
    }

    public get a() {
        return this._a;
    }

    public set x(value: number) {
        if (value < 0) {
            value += 0x100;
        }
        this._x = value & 0xff;
    }

    public get x() {
        return this._x;
    }

    public set y(value: number) {
        if (value < 0) {
            value += 0x100;
        }
        this._y = value & 0xff;
    }

    public get y() {
        return this._y;
    }

    public set pc(value: number) {
        if (value < 0) {
            value += 0x10000;
        }
        this._pc = value & 0xffff;
    }

    public get pc() {
        return this._pc;
    }

    public set sp(value: number) {
        if (value < 0) {
            value += 0x100;
        }
        this._sp = value & 0xff;
    }

    public get sp() {
        return this._sp;
    }

    public setFlag(flag: Flags, value: boolean) {
        if (value) {
            this.status |= (1 << flag);
        } else {
            this.status &= ~(1 << flag);
        }
    }

    public getFlag(flag: Flags): 0 | 1 {
        return ((this.status & (1 << flag)) > 0) ? 1 : 0;
    }
}

export enum Flags {
    CARRY = 0,
    ZERO = 1,
    IRQB = 2,
    DEC = 3,
    BRK = 4,
    UNUSED = 5,
    OVERFLOW = 6,
    NEGATIVE = 7
}

export enum InterruptType {
    NONE,
    IRQ,
    NMI
}