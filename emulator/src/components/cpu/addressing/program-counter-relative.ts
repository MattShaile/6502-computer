import { C6502 } from "components/cpu/c6502";
import { AddressMode } from "./address-mode";

export class ProgramCounterRelative extends AddressMode {
    // Single byte operand is added to program counter
    public read(cpu: C6502): number {
        const operand = this.fetchOperand(cpu);

        // 2's compliment for -128 to 127 offset
        // Remember to add the pc which would have happened after this instruction, in the case of a negative jump
        return cpu.reg.pc + ((operand < 128) ? (operand) : ((-(255 - operand)) - 1));
    }
}