import { Hex } from "util/hex";
import { C6502 } from "../c6502";

// TODO: Abstract address mode, each address mode will have it's own implementation
export abstract class AddressMode {

    // A page boundary was crossed
    public pageBoundaryCrossed: boolean = false;

    // Log of last operand this address mode got
    protected lastOperands: string = "     ";

    constructor(
        // Number of bytes after the opcode to take as operands
        protected operandBytes: number
    ) { };

    // Get value at the memory address this mode points to
    public get(cpu: C6502): void {
        throw new Error("Address mode not implemented");
    }

    public read(cpu: C6502): number {
        this.get(cpu);
        return cpu.read();
    }

    public getOperands() {
        return this.lastOperands;
    }

    protected fetchOperand(cpu: C6502) {
        if (this.operandBytes === 0) {
            throw new Error("This address mode has no operands");
        }

        const operands: number[] = [];
        for (let i = 0; i < this.operandBytes; i++) {
            cpu.reg.pc++;
            cpu.address = cpu.reg.pc;
            cpu.read();
            operands.push(cpu.data);
        }

        let output = operands[0];
        this.lastOperands = `${Hex(operands[0], 2, "")}   `;

        if (operands.length > 1) {
            // Little endian conversion
            output = operands[0] + (operands[1] << 8);
            this.lastOperands = `${Hex(operands[0], 2, "")} ${Hex(operands[1], 2, "")}`;
        }

        return output;
    }
}
