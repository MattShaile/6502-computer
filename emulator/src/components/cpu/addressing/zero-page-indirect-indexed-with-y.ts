import { C6502 } from "components/cpu/c6502";
import { Hex } from "util/hex";
import { AbsoluteIndexedIndirect } from "./absolute-indexed-indirect";

export class ZeroPageIndirectIndexedWithY extends AbsoluteIndexedIndirect {
    // Single byte address (zero page), is read, then Y is added to form a pointer, this pointer is then read and returned
    public get(cpu: C6502): void {
        const operand = this.fetchOperand(cpu);
        cpu.address = operand;

        // Read 2 bytes from memory at this address
        const pointerLowByte = cpu.read();

        cpu.address = (cpu.address + 1) & 0xff;
        const pointerHighByte = cpu.read();

        // Address pointed to by operand (and operand + 1)
        let effectiveAddress = pointerLowByte + (pointerHighByte << 8);

        // Check for page boundary cross
        this.pageBoundaryCrossed = !!((effectiveAddress & 0xff00) !== ((effectiveAddress + cpu.reg.y) & 0xff00));

        // Add Y
        effectiveAddress += cpu.reg.y;

        if (effectiveAddress >= 0x10000) {
            effectiveAddress -= 0x10000;
        }

        // Read from final address
        cpu.address = effectiveAddress;
    }
}