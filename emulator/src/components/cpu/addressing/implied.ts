import { C6502 } from "components/cpu/c6502";
import { AddressMode } from "./address-mode";

export class Implied extends AddressMode {
    // Address implied with operation, so do nothing
    public address(cpu: C6502): number { return null; }
}