import { C6502 } from "components/cpu/c6502";
import { AddressMode } from "./address-mode";

export class Stack extends AddressMode {
    // Will use the stack pointer with the command, so effectively an implied address, do nothing
    public get(cpu: C6502): number { return null; }
}