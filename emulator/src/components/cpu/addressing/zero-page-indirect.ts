import { C6502 } from "components/cpu/c6502";
import { AddressMode } from "./address-mode";

export class ZeroPageIndirect extends AddressMode {
    // Operands are used to lookup 2 bytes at an address which is then used as an absolute address
    public read(cpu: C6502): number {
        const operand = this.fetchOperand(cpu);
        cpu.address = operand;

        // Read 2 bytes from memory at this address, to get a new pointer
        const pointerLowByte = cpu.read();
        cpu.address++;
        const pointerHighByte = cpu.read();
        const effectiveAddress = pointerLowByte + (pointerHighByte << 8);

        // Read 2 bytes from the new pointer
        cpu.address = effectiveAddress;
        const finalLowByte = cpu.read();
        cpu.address++;
        const finalHighByte = cpu.read();

        return finalLowByte + (finalHighByte << 8);
    }
}