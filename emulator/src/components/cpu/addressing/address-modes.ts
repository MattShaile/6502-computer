import { Absolute } from "./absolute";
import { AbsoluteIndexedIndirect } from "./absolute-indexed-indirect";
import { AbsoluteIndexedWithX } from "./absolute-indexed-with-x";
import { AbsoluteIndexedWithY } from "./absolute-indexed-with-y";
import { AbsoluteIndirect } from "./absolute-indirect";
import { Accumulator } from "./accumulator";
import { Immediate } from "./immediate";
import { Implied } from "./implied";
import { ProgramCounterRelative } from "./program-counter-relative";
import { Stack } from "./stack";
import { ZeroPage } from "./zero-page";
import { ZeroPageIndexedIndirect } from "./zero-page-indexed-indirect";
import { ZeroPageIndexedWithX } from "./zero-page-indexed-with-x";
import { ZeroPageIndexedWithY } from "./zero-page-indexed-with-y";
import { ZeroPageIndirect } from "./zero-page-indirect";
import { ZeroPageIndirectIndexedWithY } from "./zero-page-indirect-indexed-with-y";
import { ZeroPageRelative } from "./zero-page-relative";

// List of all possible address modes
export const AddressModes = {
    "a": new Absolute(2),
    "(a,x)": new AbsoluteIndexedIndirect(2),
    "a,x": new AbsoluteIndexedWithX(2),
    "a,y": new AbsoluteIndexedWithY(2),
    "(a)": new AbsoluteIndirect(2),
    "A": new Accumulator(0),
    "#": new Immediate(1),
    "i": new Implied(0),
    "r": new ProgramCounterRelative(1),
    "s": new Stack(0),
    "zp": new ZeroPage(1),
    "(zp,x)": new ZeroPageIndexedIndirect(1),
    "zp,x": new ZeroPageIndexedWithX(1),
    "zp,y": new ZeroPageIndexedWithY(1),
    "(zp)": new ZeroPageIndirect(1),
    "(zp),y": new ZeroPageIndirectIndexedWithY(1),
    "zp,r": new ZeroPageRelative(2)
}