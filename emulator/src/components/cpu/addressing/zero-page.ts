import { Absolute } from "./absolute";

// Essentially the same as absolute, except it's a single byte address (the zero page)  
export class ZeroPage extends Absolute {
}