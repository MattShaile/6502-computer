import { C6502 } from "components/cpu/c6502";
import { AddressMode } from "./address-mode";

export class Absolute extends AddressMode {
    // Absolute address   
    public get(cpu: C6502): void {
        const operand = this.fetchOperand(cpu);
        cpu.address = operand;
    }
}