import { C6502 } from "components/cpu/c6502";
import { AddressMode } from "./address-mode";

export class Immediate extends AddressMode {
    // Immediate, so the operand is literally a direct number
    public read(cpu: C6502): number {
        const operand = this.fetchOperand(cpu);

        return operand;
    }
}