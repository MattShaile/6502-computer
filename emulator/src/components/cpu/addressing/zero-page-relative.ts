import { Absolute } from "./absolute";
import { AddressMode } from "./address-mode";
import { Immediate } from "./immediate";

// This is a special address mode only used by BBRX and BBSX instructions, see those classes for details
export class ZeroPageRelative extends Immediate {
}