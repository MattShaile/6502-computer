import { C6502 } from "components/cpu/c6502";
import { AddressMode } from "./address-mode";

export class AbsoluteIndirect extends AddressMode {
    // Operands are used to lookup 2 bytes at an address which is then used as an absolute address
    public read(cpu: C6502): number {
        const operand = this.fetchOperand(cpu);
        cpu.address = operand;

        // Read 2 bytes from memory at this address, to get a new pointer
        const indirectLowByte = cpu.read();
        // The indirect jump instruction does not increment the page address when the indirect pointer crosses a page boundary. JMP ($xxFF) will fetch the address from $xxFF and $xx00.
        cpu.address = (cpu.address & 0xff00) + ((cpu.address + 1) & 0xff);
        const indirectHighByte = cpu.read();
        const indirectAddress = indirectLowByte + (indirectHighByte << 8);

        return indirectAddress;
    }
}