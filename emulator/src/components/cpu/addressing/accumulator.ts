import { C6502 } from "components/cpu/c6502";
import { AddressMode } from "./address-mode";

export class Accumulator extends AddressMode {
    // Implied as accumulator, so do nothing  
    public get(cpu: C6502): void { return null; }
}