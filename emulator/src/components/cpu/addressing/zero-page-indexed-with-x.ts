import { C6502 } from "components/cpu/c6502";
import { AddressMode } from "./address-mode";

export class ZeroPageIndexedWithX extends AddressMode {
    // Absolute, plus x
    public get(cpu: C6502): void {
        // Add x to operand
        const operand = this.fetchOperand(cpu);
        cpu.address = (operand + cpu.reg.x) & 0xff;
    }
}