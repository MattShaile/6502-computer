import { C6502 } from "components/cpu/c6502";
import { AddressMode } from "./address-mode";

export class AbsoluteIndexedWithX extends AddressMode {
    // Absolute, plus x
    public get(cpu: C6502): void {
        // Add x to operand
        const operand = this.fetchOperand(cpu);
        cpu.address = operand + cpu.reg.x;

        // Check for page boundary crossed
        this.pageBoundaryCrossed = !!((cpu.address & 0xff00) !== (operand & 0xff00));
    }
}