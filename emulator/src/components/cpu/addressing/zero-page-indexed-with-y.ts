import { C6502 } from "components/cpu/c6502";
import { AddressMode } from "./address-mode";

export class ZeroPageIndexedWithY extends AddressMode {
    // Absolute, plus y
    public get(cpu: C6502): void {
        // Add y to operand
        const operand = this.fetchOperand(cpu);
        cpu.address = (operand + cpu.reg.y) & 0xff;
    }
}