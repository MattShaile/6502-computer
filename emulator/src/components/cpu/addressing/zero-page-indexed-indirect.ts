import { C6502 } from "components/cpu/c6502";
import { AddressMode } from "./address-mode";

export class ZeroPageIndexedIndirect extends AddressMode {
    // Special 65c02 addressing mode, see data sheet
    public get(cpu: C6502): void {
        // Add x to operand
        const operand = this.fetchOperand(cpu);
        cpu.address = (operand + cpu.reg.x) & 0xff;

        // Read 2 bytes from memory at this address, to get a new pointer
        const pointerLowByte = cpu.read();
        cpu.address = (cpu.address + 1) & 0xff;
        const pointerHighByte = cpu.read();
        const targetAddress = pointerLowByte + (pointerHighByte << 8);

        cpu.address = targetAddress;
    }
}