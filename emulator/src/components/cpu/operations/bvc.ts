import { C6502, Flags } from "components/cpu/c6502";
import { Operation } from "./operations";

export class BVC extends Operation {
    public execute(cpu: C6502) {
        const operand = this.addressMode.read(cpu);

        if (!cpu.reg.getFlag(Flags.OVERFLOW)) {
            this.cyclesTaken++;
            cpu.reg.pc = operand;
        }

        return this.log(cpu);
    }
}
