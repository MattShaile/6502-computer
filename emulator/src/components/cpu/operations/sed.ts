import { C6502, Flags } from "components/cpu/c6502";
import { Operation } from "./operations";

export class SED extends Operation {
    public execute(cpu: C6502) {
        cpu.reg.setFlag(Flags.DEC, true);
    
        return this.log(cpu);
    }
}
