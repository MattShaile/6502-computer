import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class RTI extends Operation {
    public execute(cpu: C6502) {
        cpu.reg.sp++;

        // Pop stored flags off stack
        cpu.address = cpu.reg.sp + 0x100;
        const flags = cpu.read();

        cpu.reg.sp++;

        // Pop 16 bit pc off stack
        cpu.address = cpu.reg.sp + 0x100;
        const lowerByte = cpu.read();

        cpu.reg.sp++;

        cpu.address = cpu.reg.sp + 0x100;
        const upperByte = cpu.read();

        const returnAddress = lowerByte + (upperByte << 8);

        // Restore PC
        cpu.reg.pc = returnAddress - 1;

        // Restore flags
        cpu.reg.setStatusFromByte(flags);
        
    
        return this.log(cpu);
    }
}
