import { C6502 } from "components/cpu/c6502";
import { Hex } from "util/hex";
import { Absolute } from "../addressing/absolute";
import { Operation } from "./operations";

export class JMP extends Operation {
    public execute(cpu: C6502) {
        let address = this.addressMode.read(cpu);

        // When address mode is absolute, JMP refers to the address referenced by the operands, not the value at that memory location
        if (this.addressMode instanceof Absolute) {
            address = cpu.address;
        }

        // Jump back 1 less since pc will be increased at the end of the instruction
        cpu.reg.pc = address - 1;

        return this.log(cpu);
    }
}
