import { C6502, Flags } from "components/cpu/c6502";
import { Operation } from "./operations";

export class SBC extends Operation {
    public execute(cpu: C6502) {
        const operand = this.addressMode.read(cpu);

        const result = cpu.reg.a - operand - ((cpu.reg.getFlag(Flags.CARRY) ? 0 : 1));
        const croppedResult = result & 0xff;

        // Formula from http://www.righto.com/2012/12/the-6502-overflow-flag-explained.html
        cpu.reg.setFlag(Flags.OVERFLOW, !!((cpu.reg.a ^ croppedResult) & ((0xff - operand) ^ croppedResult) & 0x80));

        cpu.reg.a = croppedResult;

        cpu.reg.setFlag(Flags.ZERO, cpu.reg.a === 0);
        cpu.reg.setFlag(Flags.NEGATIVE, !!(cpu.reg.a & 0b10000000));
        cpu.reg.setFlag(Flags.CARRY, result >= 0);
    
        return this.log(cpu);
    }
}