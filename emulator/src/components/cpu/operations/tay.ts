import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class TAY extends Operation {
    public execute(cpu: C6502) {
        cpu.reg.y = cpu.reg.a;

        this.setFlags(cpu, cpu.reg.y);
    
        return this.log(cpu);
    }
}
