import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class STX extends Operation {
    public execute(cpu: C6502) {
        this.addressMode.get(cpu);
        cpu.data = cpu.reg.x;
        cpu.write();
    
        return this.log(cpu);
    }
}
