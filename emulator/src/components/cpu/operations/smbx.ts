import { AddressMode } from "components/cpu/addressing/address-mode";
import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class SMBX extends Operation {

    constructor(protected bit: number, addressMode: AddressMode, cycles: number) {
        super(addressMode, cycles);
    }

    public execute(cpu: C6502) {
        let data = this.addressMode.read(cpu);
        data = data | (1 << this.bit);
        cpu.data = data;
        cpu.write();

        this.setFlags(cpu, data);

        return this.log(cpu);
    }
}
