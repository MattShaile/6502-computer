import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class JSR extends Operation {
    public execute(cpu: C6502) {

        const address = this.addressMode.get(cpu);
        const jumpAddress = cpu.address;

        // TODO: Potentially move this logic to stack address mode?
        const lowerByte = cpu.reg.pc & 0xff;
        const upperByte = cpu.reg.pc >> 8;

        cpu.address = cpu.reg.sp + 0x100;
        cpu.data = upperByte;
        cpu.write();
        cpu.reg.sp--;
        cpu.address = cpu.reg.sp + 0x100;
        cpu.data = lowerByte;
        cpu.write();
        cpu.reg.sp--;

        // Jump back 1 less since pc will be increased at the end of the instruction
        cpu.reg.pc = jumpAddress - 1;
    
        return this.log(cpu);
    }
}
