import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class LDX extends Operation {
    public execute(cpu: C6502) {
        const operand = this.addressMode.read(cpu);
        cpu.reg.x = operand;

        this.setFlags(cpu, cpu.reg.x);
    
        return this.log(cpu);
    }
}
