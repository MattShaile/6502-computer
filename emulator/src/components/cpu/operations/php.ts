import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class PHP extends Operation {
    public execute(cpu: C6502) {
        cpu.address = cpu.reg.sp + 0x100;
        cpu.data = cpu.reg.status;
        cpu.write();
        cpu.reg.sp--;
    
        return this.log(cpu);
    }
}
