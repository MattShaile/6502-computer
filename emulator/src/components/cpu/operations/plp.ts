import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class PLP extends Operation {
    public execute(cpu: C6502) {
        cpu.reg.sp++;
        cpu.address = cpu.reg.sp + 0x100;
        cpu.reg.status = cpu.read();
    
        return this.log(cpu);
    }
}
