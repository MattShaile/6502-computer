import { C6502 } from "components/cpu/c6502";
import { AddressMode } from "../addressing/address-mode";
import { Operation } from "./operations";

export class BBRX extends Operation {

    protected testPass: boolean = false;

    constructor(protected bit: number, addressMode: AddressMode, cycles: number) {
        super(addressMode, cycles);
    }

    public execute(cpu: C6502) {
        const operand = this.addressMode.read(cpu);

        // Operand will contain the next 2 bytes as a single 16 bit number to keep addressing implementation simple
        // So we need to split them back up
        const zeroPageAddress = operand & 0xff;
        const relativeJump = operand >> 8;

        cpu.address = zeroPageAddress;
        const testValue = cpu.read();

        if (!!(testValue & (1 << this.bit)) === this.testPass) {
            // 2's compliment for -128 to 127 offset
            // Remember to add the pc which would have happened after this instruction, in the case of a negative jump
            cpu.reg.pc = cpu.reg.pc + (relativeJump < 128 ? relativeJump : ((-(255 - relativeJump)) - 1));
        }

        return this.log(cpu);
    }
}
