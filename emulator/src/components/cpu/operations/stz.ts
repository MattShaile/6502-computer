import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class STZ extends Operation {
    public execute(cpu: C6502) {
        this.addressMode.get(cpu);
        cpu.data = 0;
        cpu.write();
    
        return this.log(cpu);
    }
}
