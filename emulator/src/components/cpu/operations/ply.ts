import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class PLY extends Operation {
    public execute(cpu: C6502) {
        cpu.reg.sp++;
        cpu.address = cpu.reg.sp + 0x100;
        cpu.reg.y = cpu.read();
        this.setFlags(cpu, cpu.reg.y);
    
        return this.log(cpu);
    }
}
