import { C6502 } from "components/cpu/c6502";
import { BBRX } from "./bbrX";

export class BBSX extends BBRX {

    public execute(cpu: C6502) {

        this.testPass = true;

        return super.execute(cpu);

        return this.log(cpu);
    }
}
