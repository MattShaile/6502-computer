import { C6502, Flags } from "components/cpu/c6502";
import { Operation } from "./operations";

export class CLC extends Operation {
    public execute(cpu: C6502) {
        cpu.reg.setFlag(Flags.CARRY, false);

        return this.log(cpu);
    }
}
