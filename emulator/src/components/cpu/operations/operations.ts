import { AddressMode } from "../addressing/address-mode";
import { C6502, Flags } from "../c6502";

export abstract class Operation {


    protected name: string;
    protected cyclesTaken: number;

    constructor(protected addressMode: AddressMode, protected cycles: number) {
        this.cyclesTaken = cycles;
        this.name = this.constructor.name;
    }

    // Executes the operation and returns a log
    public execute(cpu: C6502): OperationLog {
        throw new Error("Opcode not implemented");
    }

    protected setFlags(cpu: C6502, result: number) {
        cpu.reg.setFlag(Flags.ZERO, result === 0);
        cpu.reg.setFlag(Flags.NEGATIVE, !!(result & 0b10000000));
    }

    protected log(cpu: C6502) {
        let totalCycles = this.cyclesTaken;
        // STA is a special exception and doesn't take a cycle penalty for crossing a page boundary
        if (!(this.name === "STA")) {
            totalCycles += (this.addressMode.pageBoundaryCrossed ? 1 : 0);
        }

        const output = {
            opName: this.name,
            operands: this.addressMode.getOperands(),
            cycles: totalCycles
        };

        this.cyclesTaken = this.cycles;

        return output;
    }
}

export interface OperationLog {
    opName: string;
    operands: string;
    cycles: number;
}