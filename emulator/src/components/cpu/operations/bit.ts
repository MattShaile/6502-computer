import { C6502, Flags } from "components/cpu/c6502";
import { Operation } from "./operations";

export class BIT extends Operation {
    public execute(cpu: C6502) {
        const operand = this.addressMode.read(cpu);

        const result = (cpu.reg.a & operand);

        cpu.reg.setFlag(Flags.NEGATIVE, !!(operand & 0b10000000));
        cpu.reg.setFlag(Flags.OVERFLOW, !!(operand & 0b01000000));
        cpu.reg.setFlag(Flags.ZERO, result === 0);

        return this.log(cpu);
    }
}
