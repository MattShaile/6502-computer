import { C6502, Flags } from "components/cpu/c6502";
import { Operation } from "./operations";

export class CLV extends Operation {
    public execute(cpu: C6502) {
        cpu.reg.setFlag(Flags.OVERFLOW, false);

        return this.log(cpu);
    }
}
