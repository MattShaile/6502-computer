import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class TSX extends Operation {
    public execute(cpu: C6502) {
        cpu.reg.x = cpu.reg.sp;

        this.setFlags(cpu, cpu.reg.x);
    
        return this.log(cpu);
    }
}
