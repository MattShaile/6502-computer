import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class AND extends Operation {
    public execute(cpu: C6502) {
        const operand = this.addressMode.read(cpu);

        const result = (cpu.reg.a & operand);

        cpu.reg.a = result;

        this.setFlags(cpu, cpu.reg.a);

        return this.log(cpu);
    }
}
