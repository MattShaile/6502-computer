import { Accumulator } from "components/cpu/addressing/accumulator";
import { C6502, Flags } from "components/cpu/c6502";
import { Operation } from "./operations";

export class ROR extends Operation {
    public execute(cpu: C6502) {
        let operand;
        // Read the target to rotate
        if (this.addressMode instanceof Accumulator) {
            operand = cpu.reg.a;
        } else {
            operand = this.addressMode.read(cpu);
        }

        // Set carry flag
        const oldCarry = cpu.reg.getFlag(Flags.CARRY);
        cpu.reg.setFlag(Flags.CARRY, !!(operand & 1));

        // Rotate
        let result = (operand >> 1);
        result += (oldCarry << 7);
        result &= 0xff;

        // Set N and Z flags
        cpu.reg.setFlag(Flags.ZERO, result === 0);
        cpu.reg.setFlag(Flags.NEGATIVE, !!oldCarry);

        // Write result
        if (this.addressMode instanceof Accumulator) {
            cpu.reg.a = result;
        } else {
            cpu.data = result;
            cpu.write();
        }
    
        return this.log(cpu);
    }
}
