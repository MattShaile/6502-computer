import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class DEX extends Operation {
    public execute(cpu: C6502) {
        cpu.reg.x --;

        this.setFlags(cpu, cpu.reg.x);
    
        return this.log(cpu);
    }
}
