import { Accumulator } from "components/cpu/addressing/accumulator";
import { C6502, Flags } from "components/cpu/c6502";
import { Operation } from "./operations";

export class LSR extends Operation {
    public execute(cpu: C6502) {
        if (this.addressMode instanceof Accumulator) {
            cpu.reg.setFlag(Flags.CARRY, !!(cpu.reg.a & 1));
            cpu.reg.a = cpu.reg.a >> 1;
            cpu.reg.setFlag(Flags.ZERO, cpu.reg.a === 0);
        } else {
            let data = this.addressMode.read(cpu);
            cpu.reg.setFlag(Flags.CARRY, !!(data & 1));
            data = data >> 1;
            cpu.reg.setFlag(Flags.ZERO, data === 0);
            cpu.data = data;
            cpu.write();
        }

        cpu.reg.setFlag(Flags.NEGATIVE, false);
    
        return this.log(cpu);
    }
}
