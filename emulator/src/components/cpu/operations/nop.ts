import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class NOP extends Operation {
    public execute(cpu: C6502) { 
        return this.log(cpu);
    }// Does nothing
}
