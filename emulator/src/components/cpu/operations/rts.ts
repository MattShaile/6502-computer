import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class RTS extends Operation {
    public execute(cpu: C6502) {
        cpu.reg.sp++;

        cpu.address = cpu.reg.sp + 0x100;
        const lowerByte = cpu.read();

        cpu.reg.sp++;

        cpu.address = cpu.reg.sp + 0x100;
        const upperByte = cpu.read();

        const returnAddress = lowerByte + (upperByte << 8);

        cpu.reg.pc = returnAddress;
    
        return this.log(cpu);
    }
}
