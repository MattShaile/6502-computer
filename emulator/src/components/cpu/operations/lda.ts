import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class LDA extends Operation {
    public execute(cpu: C6502) {
        const operand = this.addressMode.read(cpu);
        cpu.reg.a = operand;

        this.setFlags(cpu, cpu.reg.a);

        return this.log(cpu);
    }
}
