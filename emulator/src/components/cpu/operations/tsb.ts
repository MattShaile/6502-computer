import { C6502, Flags } from "components/cpu/c6502";
import { Operation } from "./operations";

export class TSB extends Operation {
    public execute(cpu: C6502) {
        const operand = this.addressMode.read(cpu);

        // Read byte from memory
        cpu.address = operand;
        let result = cpu.read();

        // Logical or with A
        result = cpu.reg.a | result;

        // Write back to memory
        cpu.data = result;
        cpu.write();

        // Set or reset zero flag
        cpu.reg.setFlag(Flags.ZERO, result === 0)
    
        return this.log(cpu);
    }
}
