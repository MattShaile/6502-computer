import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class TXA extends Operation {
    public execute(cpu: C6502) {
        cpu.reg.a = cpu.reg.x;

        this.setFlags(cpu, cpu.reg.a);
    
        return this.log(cpu);
    }
}
