import { C6502, Flags } from "components/cpu/c6502";
import { Operation } from "./operations";

export class CMP extends Operation {
    public execute(cpu: C6502) {
        const operand = this.addressMode.read(cpu);
        const comparison = this.getRegisterToCompare(cpu);

        const result = comparison - operand;

        cpu.reg.setFlag(Flags.ZERO, result === 0);
        cpu.reg.setFlag(Flags.NEGATIVE, !!(result & 0b10000000));
        cpu.reg.setFlag(Flags.CARRY, operand <= comparison);

        return this.log(cpu);
    }

    protected getRegisterToCompare(cpu: C6502): number {
        return cpu.reg.a;
    }
}
