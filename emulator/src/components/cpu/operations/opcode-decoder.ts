import { AddressModes } from "components/cpu/addressing/address-modes";
import { Hex } from "util/hex";
import { ADC } from "./adc";
import { AND } from "./and";
import { ASL } from "./asl";
import { BBRX } from "./bbrX";
import { BBSX } from "./bbsX";
import { BCC } from "./bcc";
import { BCS } from "./bcs";
import { BEQ } from "./beq";
import { BIT } from "./bit";
import { BMI } from "./bmi";
import { BNE } from "./bne";
import { BPL } from "./bpl";
import { BRA } from "./bra";
import { BVC } from "./bvc";
import { BVS } from "./bvs";
import { CLC } from "./clc";
import { CLD } from "./cld";
import { CLI } from "./cli";
import { CLV } from "./clv";
import { CMP } from "./cmp";
import { CPX } from "./cpx";
import { CPY } from "./cpy";
import { DEC } from "./dec";
import { DEX } from "./dex";
import { DEY } from "./dey";
import { EOR } from "./eor";
import { INC } from "./inc";
import { INX } from "./inx";
import { INY } from "./iny";
import { JMP } from "./jmp";
import { JSR } from "./jsr";
import { LDA } from "./lda";
import { LDX } from "./ldx";
import { LDY } from "./ldy";
import { LSR } from "./lsr";
import { NOP } from "./nop";
import { Operation } from "./operations";
import { ORA } from "./ora";
import { PHA } from "./pha";
import { PHP } from "./php";
import { PHX } from "./phx";
import { PHY } from "./phy";
import { PLA } from "./pla";
import { PLP } from "./plp";
import { PLX } from "./plx";
import { PLY } from "./ply";
import { RMBX } from "./rmbx";
import { ROL } from "./rol";
import { ROR } from "./ror";
import { RTI } from "./rti";
import { RTS } from "./rts";
import { SBC } from "./sbc";
import { SEC } from "./sec";
import { SED } from "./sed";
import { SEI } from "./sei";
import { SMBX } from "./smbx";
import { STA } from "./sta";
import { STX } from "./stx";
import { STY } from "./sty";
import { STZ } from "./stz";
import { TAX } from "./tax";
import { TAY } from "./tay";
import { TRB } from "./trb";
import { TSB } from "./tsb";
import { TSX } from "./tsx";
import { TXA } from "./txa";
import { TXS } from "./txs";
import { TYA } from "./tya";

export class OpcodeDecoder {
    // Decode an opcode and return an operation
    public decode(opcode: number): Operation {
        const operation: Operation = OpcodeTable[opcode];

        if (!operation) {
            throw new Error(`Invalid opcode: ${Hex(opcode)}`);
        }

        return operation;
    }
}

const OpcodeTable: Operation[] = [
    null,                                               // 0x00 - BRK - Handled by C6502
    new ORA(AddressModes["(zp,x)"], 6),                 // 0x01
    null,                                               // 0x02 - Invalid opcode
    null,                                               // 0x03 - Invalid opcode
    new TSB(AddressModes["zp"], 5),                     // 0x04
    new ORA(AddressModes["zp"], 3),                     // 0x05
    new ASL(AddressModes["zp"], 5),                     // 0x06
    new RMBX(0, AddressModes["zp"], 5),                 // 0x07
    new PHP(AddressModes["s"], 3),                      // 0x08
    new ORA(AddressModes["#"], 2),                      // 0x09
    new ASL(AddressModes["A"], 2),                      // 0x0a
    null,                                               // 0x0b - Invalid opcode
    new TSB(AddressModes["a"], 6),                      // 0x0c
    new ORA(AddressModes["a"], 4),                      // 0x0d
    new ASL(AddressModes["a"], 6),                      // 0x0e
    new BBRX(0, AddressModes["zp,r"], 5),               // 0x0f
    new BPL(AddressModes["r"], 2),                      // 0x10
    new ORA(AddressModes["(zp),y"], 5),                 // 0x11
    new ORA(AddressModes["(zp)"], 5),                   // 0x12
    null,                                               // 0x13 - Invalid opcode
    new TRB(AddressModes["zp"], 5),                     // 0x14
    new ORA(AddressModes["zp,x"], 4),                   // 0x15
    new ASL(AddressModes["zp,x"], 6),                   // 0x16
    new RMBX(1, AddressModes["zp"], 5),                 // 0x17
    new CLC(AddressModes["i"], 2),                      // 0x18
    new ORA(AddressModes["a,y"], 4),                    // 0x19
    new INC(AddressModes["A"], 2),                      // 0x1a
    null,                                               // 0x1b - Invalid opcode
    new TRB(AddressModes["a"], 6),                      // 0x1c
    new ORA(AddressModes["a,x"], 4),                    // 0x1d
    new ASL(AddressModes["a,x"], 7),                    // 0x1e
    new BBRX(1, AddressModes["zp,r"], 5),               // 0x1f
    new JSR(AddressModes["a"], 6),                      // 0x20
    new AND(AddressModes["(zp,x)"], 6),                 // 0x21
    null,                                               // 0x22 - Invalid opcode
    null,                                               // 0x23 - Invalid opcode
    new BIT(AddressModes["zp"], 3),                     // 0x24
    new AND(AddressModes["zp"], 3),                     // 0x25
    new ROL(AddressModes["zp"], 5),                     // 0x26
    new RMBX(2, AddressModes["zp"], 5),                 // 0x27
    new PLP(AddressModes["s"], 4),                      // 0x28
    new AND(AddressModes["#"], 2),                      // 0x29
    new ROL(AddressModes["A"], 2),                      // 0x2a
    null,                                               // 0x2b - Invalid opcode
    new BIT(AddressModes["a"], 4),                      // 0x2c
    new AND(AddressModes["a"], 4),                      // 0x2d
    new ROL(AddressModes["a"], 6),                      // 0x2e
    new BBRX(2, AddressModes["zp,r"], 5),               // 0x2f
    new BMI(AddressModes["r"], 2),                      // 0x30
    new AND(AddressModes["(zp),y"], 5),                 // 0x31
    new AND(AddressModes["(zp)"], 5),                   // 0x32
    null,                                               // 0x33 - Invalid opcode
    new BIT(AddressModes["zp,x"], 3),                   // 0x34
    new AND(AddressModes["zp,x"], 4),                   // 0x35
    new ROL(AddressModes["zp,x"], 6),                   // 0x36
    new RMBX(3, AddressModes["zp"], 5),                 // 0x37
    new SEC(AddressModes["i"], 2),                      // 0x38
    new AND(AddressModes["a,y"], 4),                    // 0x39
    new DEC(AddressModes["A"], 2),                      // 0x3a
    null,                                               // 0x3b - Invalid opcode
    new BIT(AddressModes["a,x"], 4),                    // 0x3c
    new AND(AddressModes["a,x"], 4),                    // 0x3d
    new ROL(AddressModes["a,x"], 7),                    // 0x3e
    new BBRX(3, AddressModes["zp,r"], 5),               // 0x3f
    new RTI(AddressModes["i"], 6),                      // 0x40
    new EOR(AddressModes["(zp,x)"], 6),                 // 0x41
    null,                                               // 0x42 - Invalid opcode
    null,                                               // 0x43 - Invalid opcode
    null,                                               // 0x44 - Invalid opcode
    new EOR(AddressModes["zp"], 3),                     // 0x45
    new LSR(AddressModes["zp"], 5),                     // 0x46
    new RMBX(4, AddressModes["zp"], 5),                 // 0x47
    new PHA(AddressModes["s"], 3),                      // 0x48
    new EOR(AddressModes["#"], 2),                      // 0x49
    new LSR(AddressModes["A"], 2),                      // 0x4a
    null,                                               // 0x4b - Invalid opcode
    new JMP(AddressModes["a"], 3),                      // 0x4c
    new EOR(AddressModes["a"], 4),                      // 0x4d
    new LSR(AddressModes["a"], 6),                      // 0x4e
    new BBRX(4, AddressModes["zp,r"], 5),               // 0x4f
    new BVC(AddressModes["r"], 2),                      // 0x50
    new EOR(AddressModes["(zp),y"], 5),                 // 0x51
    new EOR(AddressModes["(zp)"], 5),                   // 0x52
    null,                                               // 0x53 - Invalid opcode
    null,                                               // 0x54 - Invalid opcode
    new EOR(AddressModes["zp,x"], 4),                   // 0x55
    new LSR(AddressModes["zp,x"], 6),                   // 0x56
    new RMBX(5, AddressModes["zp"], 5),                 // 0x57
    new CLI(AddressModes["i"], 2),                      // 0x58
    new EOR(AddressModes["a,y"], 4),                    // 0x59
    new PHY(AddressModes["s"], 3),                      // 0x5a
    null,                                               // 0x5b - Invalid opcode
    null,                                               // 0x5c - Invalid opcode
    new EOR(AddressModes["a,x"], 4),                    // 0x5d
    new LSR(AddressModes["a,x"], 7),                    // 0x5e
    new BBRX(5, AddressModes["zp,r"], 5),               // 0x5f
    new RTS(AddressModes["s"], 6),                      // 0x60
    new ADC(AddressModes["(zp,x)"], 6),                 // 0x61
    null,                                               // 0x62 - Invalid opcode
    null,                                               // 0x63 - Invalid opcode
    new STZ(AddressModes["zp"], 3),                     // 0x64
    new ADC(AddressModes["zp"], 3),                     // 0x65
    new ROR(AddressModes["zp"], 5),                     // 0x66
    new RMBX(6, AddressModes["zp"], 5),                 // 0x67
    new PLA(AddressModes["s"], 4),                      // 0x68
    new ADC(AddressModes["#"], 2),                      // 0x69
    new ROR(AddressModes["A"], 2),                      // 0x6a
    null,                                               // 0x6b - Invalid opcode
    new JMP(AddressModes["(a)"], 5),                    // 0x6c
    new ADC(AddressModes["a"], 4),                      // 0x6d
    new ROR(AddressModes["a"], 6),                      // 0x6e
    new BBRX(6, AddressModes["zp,r"], 5),               // 0x6f
    new BVS(AddressModes["r"], 2),                      // 0x70
    new ADC(AddressModes["(zp),y"], 5),                 // 0x71
    new ADC(AddressModes["(zp)"], 5),                   // 0x72
    null,                                               // 0x73 - Invalid opcode
    new STZ(AddressModes["zp,x"], 4),                   // 0x74
    new ADC(AddressModes["zp,x"], 4),                   // 0x75
    new ROR(AddressModes["zp,x"], 6),                   // 0x76
    new RMBX(7, AddressModes["zp"], 5),                 // 0x77
    new SEI(AddressModes["i"], 2),                      // 0x78
    new ADC(AddressModes["a,y"], 4),                    // 0x79
    new PLY(AddressModes["s"], 4),                      // 0x80
    null,                                               // 0x7b - Invalid opcode
    new JMP(AddressModes["(a,x)"], 6),                  // 0x7c
    new ADC(AddressModes["a,x"], 4),                    // 0x7d
    new ROR(AddressModes["a,x"], 7),                    // 0x7e
    new BBRX(7, AddressModes["zp,r"], 5),               // 0x7f
    new BRA(AddressModes["r"], 3),                      // 0x80
    new STA(AddressModes["(zp,x)"], 6),                 // 0x81
    null,                                               // 0x82 - Invalid opcode
    null,                                               // 0x83 - Invalid opcode
    new STY(AddressModes["zp"], 3),                     // 0x84
    new STA(AddressModes["zp"], 3),                     // 0x85
    new STX(AddressModes["zp"], 3),                     // 0x86
    new SMBX(0, AddressModes["zp"], 5),                 // 0x87
    new DEY(AddressModes["i"], 2),                      // 0x88
    new BIT(AddressModes["#"], 3),                      // 0x89
    new TXA(AddressModes["i"], 2),                      // 0x8a
    null,                                               // 0x8b - Invalid opcode
    new STY(AddressModes["a"], 4),                      // 0x8c
    new STA(AddressModes["a"], 4),                      // 0x8d
    new STX(AddressModes["a"], 4),                      // 0x8e
    new BBSX(0, AddressModes["zp,r"], 5),               // 0x8f
    new BCC(AddressModes["r"], 2),                      // 0x90
    new STA(AddressModes["(zp),y"], 6),                 // 0x91
    new STA(AddressModes["(zp)"], 5),                   // 0x92
    null,                                               // 0x93 - Invalid opcode
    new STY(AddressModes["zp,x"], 4),                   // 0x94
    new STA(AddressModes["zp,x"], 4),                   // 0x95
    new STX(AddressModes["zp,y"], 4),                   // 0x96
    new SMBX(1, AddressModes["zp"], 5),                 // 0x97
    new TYA(AddressModes["i"], 2),                      // 0x98
    new STA(AddressModes["a,y"], 5),                    // 0x99
    new TXS(AddressModes["i"], 2),                      // 0x9a
    null,                                               // 0x9b - Invalid opcode
    new STZ(AddressModes["a"], 4),                      // 0x9c
    new STA(AddressModes["a,x"], 5),                    // 0x9d
    new STZ(AddressModes["a,x"], 5),                    // 0x9e
    new BBSX(1, AddressModes["zp,r"], 5),               // 0x9f
    new LDY(AddressModes["#"], 2),                      // 0xa0
    new LDA(AddressModes["(zp,x)"], 6),                 // 0xa1
    new LDX(AddressModes["#"], 2),                      // 0xa2
    null,                                               // 0xa3 - Invalid opcode
    new LDY(AddressModes["zp"], 3),                     // 0xa4
    new LDA(AddressModes["zp"], 3),                     // 0xa5
    new LDX(AddressModes["zp"], 3),                     // 0xa6
    new SMBX(2, AddressModes["zp"], 5),                 // 0xa7
    new TAY(AddressModes["i"], 2),                      // 0xa8
    new LDA(AddressModes["#"], 2),                      // 0xa9
    new TAX(AddressModes["i"], 2),                      // 0xaa
    null,                                               // 0xab - Invalid opcode
    new LDY(AddressModes["a"], 4),                      // 0xac
    new LDA(AddressModes["a"], 4),                      // 0xad
    new LDX(AddressModes["a"], 4),                      // 0xae
    new BBSX(2, AddressModes["zp,r"], 5),               // 0xaf
    new BCS(AddressModes["r"], 2),                      // 0xb0
    new LDA(AddressModes["(zp),y"], 5),                 // 0xb1
    new LDA(AddressModes["(zp)"], 5),                   // 0xb2
    null,                                               // 0xb3 - Invalid opcode
    new LDY(AddressModes["zp,x"], 4),                   // 0xb4
    new LDA(AddressModes["zp,x"], 4),                   // 0xb5
    new LDX(AddressModes["zp,y"], 4),                   // 0xb6
    new SMBX(3, AddressModes["zp"], 5),                 // 0xb7
    new CLV(AddressModes["i"], 2),                      // 0xb8
    new LDA(AddressModes["a,y"], 4),                    // 0xb9
    new TSX(AddressModes["i"], 2),                      // 0xba
    null,                                               // 0xbb - Invalid opcode
    new LDY(AddressModes["a,x"], 4),                    // 0xbc
    new LDA(AddressModes["a,x"], 4),                    // 0xbd
    new LDX(AddressModes["a,y"], 4),                    // 0xbe
    new BBSX(3, AddressModes["zp,r"], 5),               // 0xbf
    new CPY(AddressModes["#"], 2),                      // 0xc0
    new CMP(AddressModes["(zp,x)"], 6),                 // 0xc1
    null,                                               // 0xc2 - Invalid opcode
    null,                                               // 0xc3 - Invalid opcode
    new CPY(AddressModes["zp"], 3),                     // 0xc4
    new CMP(AddressModes["zp"], 3),                     // 0xc5
    new DEC(AddressModes["zp"], 5),                     // 0xc6
    new SMBX(4, AddressModes["zp"], 2),                 // 0xc7
    new INY(AddressModes["i"], 2),                      // 0xc8
    new CMP(AddressModes["#"], 2),                      // 0xc9
    new DEX(AddressModes["i"], 2),                      // 0xca
    null,                                               // 0xcb - WAI - Not implemented, but when it is, should be handled by C6502
    new CPY(AddressModes["a"], 4),                      // 0xcc
    new CMP(AddressModes["a"], 4),                      // 0xcd
    new DEC(AddressModes["a"], 6),                      // 0xce
    new BBSX(4, AddressModes["zp,r"], 5),               // 0xcf
    new BNE(AddressModes["r"], 2),                      // 0xd0
    new CMP(AddressModes["(zp),y"], 5),                 // 0xd1
    new CMP(AddressModes["(zp)"], 5),                   // 0xd2
    null,                                               // 0xd3 - Invalid opcode
    null,                                               // 0xd4 - Invalid opcode
    new CMP(AddressModes["zp,x"], 4),                   // 0xd5
    new DEC(AddressModes["zp,x"], 6),                   // 0xd6
    new SMBX(5, AddressModes["zp"], 5),                 // 0xd7
    new CLD(AddressModes["i"], 2),                      // 0xd8
    new CMP(AddressModes["a,y"], 4),                    // 0xd9
    new PHX(AddressModes["s"], 3),                      // 0xda
    null,                                               // 0xdb - STP - Stops the clock, implemented in C6502
    null,                                               // 0xdc - Invalid opcode
    new CMP(AddressModes["a,x"], 4),                    // 0xdd
    new DEC(AddressModes["a,x"], 7),                    // 0xde
    new BBSX(5, AddressModes["zp,r"], 5),               // 0xdf
    new CPX(AddressModes["#"], 2),                      // 0xe0
    new SBC(AddressModes["(zp,x)"], 6),                 // 0xe1
    null,                                               // 0xe2 - Invalid opcode
    null,                                               // 0xe3 - Invalid opcode
    new CPX(AddressModes["zp"], 3),                     // 0xe4
    new SBC(AddressModes["zp"], 3),                     // 0xe5
    new INC(AddressModes["zp"], 5),                     // 0xe6
    new SMBX(6, AddressModes["zp"], 5),                 // 0xe7
    new INX(AddressModes["i"], 2),                      // 0xe8
    new SBC(AddressModes["#"], 2),                      // 0xe9
    new NOP(AddressModes["i"], 2),                      // 0xea
    null,                                               // 0xeb - Invalid opcode
    new CPX(AddressModes["a"], 4),                      // 0xec
    new SBC(AddressModes["a"], 4),                      // 0xed
    new INC(AddressModes["a"], 6),                      // 0xee
    new BBSX(6, AddressModes["zp,r"], 5),               // 0xef
    new BEQ(AddressModes["r"], 2),                      // 0xf0
    new SBC(AddressModes["(zp),y"], 5),                 // 0xf1
    new SBC(AddressModes["(zp)"], 5),                   // 0xf2
    null,                                               // 0xf3 - Invalid opcode
    null,                                               // 0xf4 - Invalid opcode
    new SBC(AddressModes["zp,x"], 4),                   // 0xf5
    new INC(AddressModes["zp,x"], 6),                   // 0xf6
    new SMBX(7, AddressModes["zp"], 5),                 // 0xf7
    new SED(AddressModes["i"], 2),                      // 0xf8
    new SBC(AddressModes["a,y"], 4),                    // 0xf9
    new PLX(AddressModes["s"], 4),                      // 0xfa
    null,                                               // 0xfb - Invalid opcode
    null,                                               // 0xfc - Invalid opcode
    new SBC(AddressModes["a,x"], 4),                    // 0xfd
    new INC(AddressModes["a,x"], 7),                    // 0xfe
    new BBSX(7, AddressModes["zp,r"], 5),               // 0xff
];