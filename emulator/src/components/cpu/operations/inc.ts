import { Accumulator } from "components/cpu/addressing/accumulator";
import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class INC extends Operation {
    public execute(cpu: C6502) {
        let result;
        if (this.addressMode instanceof Accumulator) {
            cpu.reg.a++;
            result = cpu.reg.a;
        } else {
            let data = this.addressMode.read(cpu);
            data++;
            if (data > 0xff) {
                data = 0;
            }
            cpu.data = data;
            result = data;
            cpu.write();
        }

        this.setFlags(cpu, result);
    
        return this.log(cpu);
    }
}
