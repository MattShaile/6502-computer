import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class TYA extends Operation {
    public execute(cpu: C6502) {
        cpu.reg.a = cpu.reg.y;

        this.setFlags(cpu, cpu.reg.a);
    
        return this.log(cpu);
    }
}
