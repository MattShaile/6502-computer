import { C6502, Flags } from "components/cpu/c6502";
import { Operation } from "./operations";

export class CLI extends Operation {
    public execute(cpu: C6502) {
        cpu.reg.setFlag(Flags.IRQB, false);

        return this.log(cpu);
    }
}
