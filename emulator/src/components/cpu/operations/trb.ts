import { C6502, Flags } from "components/cpu/c6502";
import { Operation } from "./operations";

export class TRB extends Operation {
    public execute(cpu: C6502) {
        const operand = this.addressMode.read(cpu);

        // Read byte from memory
        cpu.address = operand;
        let result = cpu.read();

        // Logical and with inverted A
        result = ((~cpu.reg.a) & 0xff) & result;

        // Write back to memory
        cpu.data = result;
        cpu.write();

        // Set or reset zero flag
        cpu.reg.setFlag(Flags.ZERO, result === 0)
    
        return this.log(cpu);
    }
}
