import { C6502, Flags } from "components/cpu/c6502";
import { Operation } from "./operations";

export class BCC extends Operation {
    public execute(cpu: C6502) {
        const operand = this.addressMode.read(cpu);

        if (!cpu.reg.getFlag(Flags.CARRY)) {
            this.cyclesTaken++;
            cpu.reg.pc = operand;
        }

        return this.log(cpu);
    }
}
