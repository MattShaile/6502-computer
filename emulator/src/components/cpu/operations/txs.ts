import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class TXS extends Operation {
    public execute(cpu: C6502) {
        cpu.reg.sp = cpu.reg.x;
    
        return this.log(cpu);
    }
}
