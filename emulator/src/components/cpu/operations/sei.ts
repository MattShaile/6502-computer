import { C6502, Flags } from "components/cpu/c6502";
import { Operation } from "./operations";

export class SEI extends Operation {
    public execute(cpu: C6502) {
        cpu.reg.setFlag(Flags.IRQB, true);
    
        return this.log(cpu);
    }
}
