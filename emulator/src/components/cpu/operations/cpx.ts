import { C6502 } from "components/cpu/c6502";
import { CMP } from "./cmp";

export class CPX extends CMP {

    protected getRegisterToCompare(cpu: C6502): number {
        return cpu.reg.x;
    }
}
