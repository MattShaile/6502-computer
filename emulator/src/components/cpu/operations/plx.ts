import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class PLX extends Operation {
    public execute(cpu: C6502) {
        cpu.reg.sp++;
        cpu.address = cpu.reg.sp + 0x100;
        cpu.reg.x = cpu.read();
        this.setFlags(cpu, cpu.reg.x);
    
        return this.log(cpu);
    }
}
