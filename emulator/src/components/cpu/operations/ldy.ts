import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class LDY extends Operation {
    public execute(cpu: C6502) {
        const operand = this.addressMode.read(cpu);
        cpu.reg.y = operand;
        
        this.setFlags(cpu, cpu.reg.y);
    
        return this.log(cpu);
    }
}
