import { Accumulator } from "components/cpu/addressing/accumulator";
import { C6502, Flags } from "components/cpu/c6502";
import { Operation } from "./operations";

export class DEC extends Operation {
    public execute(cpu: C6502) {
        let operand = this.addressMode.read(cpu);

        let result = operand - 1;
        if (result < 0) {
            result += 0x100;
        }
        cpu.data = result;

        cpu.write();

        this.setFlags(cpu, cpu.data);
    
        return this.log(cpu);
    }
}
