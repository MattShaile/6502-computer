import { C6502 } from "components/cpu/c6502";
import { Operation } from "./operations";

export class BRA extends Operation {
    public execute(cpu: C6502) {
        const operand = this.addressMode.read(cpu);

        cpu.reg.pc = operand;

        return this.log(cpu);
    }
}
