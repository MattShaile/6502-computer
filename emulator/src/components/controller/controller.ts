import { C6522 } from "components/via/c6522";

export class Controller {

    protected controllerFlags: number;

    constructor(protected via: C6522) {
        via.onPortAWrite.add(() => this.onControl());

        window.addEventListener("keydown", (e) => this.keyChange(e, true))
        window.addEventListener("keyup", (e) => this.keyChange(e, false))
    }

    protected onControl() {
        const control = this.via.portRead("A");

        if (control === 0b00010000) {
            this.via.portWrite("B", this.controllerFlags);
        }
    }

    protected keyChange(e: KeyboardEvent, isDown: boolean) {
        let flags = 0;
        switch (e.key) {
            case "ArrowLeft":
                flags |= 0b00000001;
                break;
            case "ArrowUp":
                flags |= 0b00000010;
                break;
            case "ArrowRight":
                flags |= 0b00000100;
                break;
            case "ArrowDown":
                flags |= 0b00001000;
                break;
        }

        if (isDown) {
            this.controllerFlags = this.controllerFlags | flags;
        } else {
            this.controllerFlags = this.controllerFlags &= ~flags;
        }

    }

}