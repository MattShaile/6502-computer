// To write:
// 00000001 (disable draw)
// 1WWW0001 (select) [data]
// 00000000 (enable draw)

import { C6522 } from "components/via/c6522";
import { Binary } from "util/binary";
import { Hex } from "util/hex";

// WWW:
// 000 = MARA
// 100 = MARB
// 010 = MAP
// 110 = TILE
// 001 = SXA
// 101 = SXB
// 011 = SYA
// 111 = SYB


export class MattGPU {

    // Arranged as 15-bit, 0bYYYYYYYXXXXXXXX where X = screen coord upper 8 bits and Y = screen coord upper 7 bits
    protected mapRAM: number[];
    // Arranges as 16-bit, 0bOOOOOOOOYYYYXXXX where X = screen coord lower 4 bits, Y = screen coord lower 4 bits, O = mapRAM output
    protected tileRAM: number[];

    protected enableDraw: boolean;

    protected marA: number;
    protected marB: number;

    protected sxA: number;
    protected sxB: number;
    protected syA: number;
    protected syB: number;

    protected ctx: CanvasRenderingContext2D;

    protected latchControl: number;

    public static vblankCount: number = 0;
    public static vblank: boolean;

    constructor(protected via: C6522, protected canvas: HTMLCanvasElement) {
        // Initialise memory with random garbage like real life
        this.mapRAM = Array(0x4000).fill(0).map(() => Math.floor(Math.random() * 0x100));
        this.tileRAM = Array(0x10000).fill(0).map(() => Math.floor(Math.random() * 0x100));

        canvas.width = 400;
        canvas.height = 300;
        this.ctx = canvas.getContext('2d');

        this.enableDraw = true;

        this.draw();
    }

    public clock() {
        const control = this.via.portRead("A");
        const data = this.via.portRead("B");

        const gpuMode = !!(control & 0b10000000);
        this.enableDraw = !((control & 0x01000000)) && gpuMode;

        if (control != this.latchControl) {
            this.latchControl = control;

            // Is GPU mode
            if (gpuMode) {
                // Register select
                if ((control & 0b00000001)) {
                    switch ((control & 0b00001110) >> 1) {
                        case 0b000:
                            this.marA = data;
                            break;
                        case 0b001:
                            this.marB = data;
                            break;
                        case 0b010:
                            this.mapRAM[(this.marB << 8) + this.marA] = data;
                            break;
                        case 0b011:
                            this.tileRAM[(this.marB << 8) + this.marA] = data;
                            break;
                        case 0b100:
                            this.sxA = data;
                            break;
                        case 0b101:
                            this.sxB = data;
                            break;
                        case 0b110:
                            this.syA = data;
                            break;
                        case 0b111:
                            this.syB = data;
                            break;
                    }
                }
            }
        }

        // Write vblank
        (window as any).m = MattGPU;
        MattGPU.vblankCount++;
        const wholeFrame = (1000000) / 60;
        const lineTime = wholeFrame / 628;
        if (MattGPU.vblankCount > lineTime * 600) {
            // VBLANK on
            if (!MattGPU.vblank) {
                if (gpuMode) {
                    this.via.portWrite("A", 0b00100000);
                }
                MattGPU.vblank = true;
            }
        } else {
            // VBLANK off
            if (MattGPU.vblank) {
                MattGPU.vblank = false;
                if (gpuMode) {
                    this.via.portWrite("A", 0);
                }
            }
        }
        if (MattGPU.vblankCount > wholeFrame) {
            MattGPU.vblankCount = 0;
        }
    }

    public draw() {
        var id = this.ctx.getImageData(0, 0, this.canvas.width, this.canvas.height);

        const scrollX = (this.sxB << 8) + (this.sxA);
        const scrollY = (this.syB << 8) + (this.syA);

        if (this.enableDraw) {
            for (let y = 0; y < this.canvas.height; y++) {
                for (let x = 0; x < this.canvas.width; x++) {
                    const scrolledX = x + scrollX;
                    const scrolledY = y + scrollY;
                    const offset = (y * this.canvas.width + x) * 4;
                    const tile = this.mapRAM[((scrolledX & 0b111111110000) >> 4) + ((scrolledY & 0b11111110000) << 4)];
                    const color = this.tileRAM[((scrolledX & 0b1111)) + ((scrolledY & 0b1111) << 4) + (tile << 8)];
                    id.data[offset] = ((color & 0b00000111)) * (0xff / 0b111);
                    id.data[offset + 1] = ((color & 0b00111000) >> 3) * (0xff / 0b111);
                    id.data[offset + 2] = ((color & 0b11000000) >> 6) * (0xff / 0b11);
                    id.data[offset + 3] = 0xff;
                }
            }
        } else {
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        }

        this.ctx.putImageData(id, 0, 0);

        requestAnimationFrame(() => this.draw());
    }
}