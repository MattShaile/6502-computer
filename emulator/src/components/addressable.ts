import { C6502 } from "components/cpu/c6502";

export abstract class Addressable {

    constructor(
        // The CPU to control this
        protected cpu: C6502,
        // The minimum address space this is interested in
        protected addressMin: number,
        // The maximum address space this is interested in
        protected addressMax: number,
        // The number of bits to actually use when addressing, allowing you to drop bits of the top of addresses for "mirroring" emulation
        protected usedBits: number = 16
    ) {
    }

    // Reads from data bus
    public read() {
    }

    // Writes to data bus
    public write() {
    }

    // Check whether the CPU is addressing somewhere between the min and max address space this is interested in
    public inAddressSpace() {
        return this.cpu.address >= this.addressMin && this.cpu.address <= this.addressMax;
    }

    // Returns the relative memory address corresponding to the address bus
    // Relative memory address is 0 indexed from this components starting address
    // The relative address is also cropped by "useBits", useful for mirroring
    protected getRelativeAddress() {
        const mask = (1 << this.usedBits) - 1;
        return (this.cpu.address - this.addressMin) & mask;
    }
}