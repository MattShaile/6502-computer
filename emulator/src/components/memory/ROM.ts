import { RAM } from "./RAM";

// Same as RAM except it's read only
export class ROM extends RAM {

    public write() {
        // Prevent writes
    }
}