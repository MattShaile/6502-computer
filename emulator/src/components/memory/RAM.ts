import { Addressable } from "components/addressable";
import { C6502 } from "components/cpu/c6502";

export class RAM extends Addressable {

    // Contents of the memory
    protected contents: number[];

    // Clone of initial contents so the chip can be reset
    protected initialContents: number[]

    constructor(
        protected cpu: C6502,
        protected addressMin: number,
        protected addressMax: number,
        protected usedBits: number = 0,
        // The initial contents of the rom
        contents?: number[]
    ) {
        super(cpu, addressMin, addressMax, usedBits);

        // -1 for used bits will make it automatically mirror to the size of the contents
        if (usedBits === -1 && contents) {
            while ((1 << usedBits) < contents.length) {
                usedBits++;
            }
            this.usedBits = usedBits;
        }

        // Initialise the ram with all 0s if it isn't specified
        const size = this.addressMax - this.addressMin;
        if (contents) {
            this.contents = contents;
            if (this.contents.length < size) {
                this.contents.push(...Array(size - this.contents.length).fill(0));
            }
        } else {
            this.contents = Array(size).fill(0, 0, size);
        }

        this.initialContents = [...this.contents];
    }

    public reset() {
        this.contents = [...this.initialContents];
    }

    // Reads content and puts it on data bus
    public read() {
        const relativeAddress = this.getRelativeAddress();
        this.cpu.data = this.contents[relativeAddress];
    }

    // Writes data bus to contents
    public write() {
        const relativeAddress = this.getRelativeAddress();
        this.contents[relativeAddress] = this.cpu.data;
    }
}