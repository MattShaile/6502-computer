import { C6502 } from "components/cpu/c6502";
import { RAM } from "components/memory/RAM";

// Example "system on a chip" implementation as described here: http://www.6502asm.com/
// All memory space is RAM, except some special addresses:
// 0xFE: Random Number Generator
// 0xFF: A code of the last pressed Button
//      Buttons:
//      $77 = W
//      $73 = A
//      $61 = S
//      $64 = D
// 0x200 - 0x5ff:	Screen
//      Colors:
//      0: black
//      1: white
//      2: grey
//      3: red
//      4: green
//      5: blue
//      6: magenta
//      7: yellow
export class ExampleSOC extends RAM {

    protected lastKeyPressed: number = 0;

    protected ctx: CanvasRenderingContext2D;
    protected canvas: HTMLCanvasElement;

    constructor(cpu: C6502, addressMin: number, addressMax: number, usedBits: number = 0, contents?: number[]) {
        super(cpu, addressMin, addressMax, usedBits, contents);

        this.setupKeyboardListener();
        this.setupScreen();
    }

    public read() {
        const relativeAddress = this.getRelativeAddress();

        // RNG
        if (relativeAddress === 0xfe) {
            this.cpu.data = Math.floor(Math.random() * 0x100);
            return;
        }
        // Last key pressed
        if (relativeAddress === 0xff) {
            this.cpu.data = this.lastKeyPressed;
            return;
        }

        super.read();
    }

    public write() {
        const relativeAddress = this.getRelativeAddress();

        if (relativeAddress >= 0x200 && relativeAddress <= 0x5ff) {
            const screenAddress = relativeAddress - 0x200;
            const screenY = Math.floor(screenAddress / 32);
            const screenX = screenAddress - screenY * 32;
            this.drawPixel(screenX, screenY, this.getColor(this.cpu.data));
        }

        super.write();
    }

    protected setupKeyboardListener() {
        window.onkeydown = (e: any) => {
            let asciiCode = -1;
            if (e.key.length === 1) {
                asciiCode = e.key.charCodeAt(0)
            }
            if (e.key === "Enter") {
                asciiCode = 13;
            }
            if (e.key === "Backspace") {
                asciiCode = 8;
            }
            if (asciiCode > -1) {
                this.lastKeyPressed = asciiCode;
            }
        };
    }

    protected setupScreen() {
        this.canvas = document.getElementById("canvas") as HTMLCanvasElement;
        this.canvas.width = 32;
        this.canvas.height = 32;
        this.ctx = this.canvas.getContext('2d');
        this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
    }

    protected drawPixel(x: number, y: number, color: number) {
        var id = this.ctx.getImageData(0, 0, this.canvas.width, this.canvas.height);

        const offset = (y * this.canvas.width + x) * 4;
        id.data[offset] = (color >> 16) & 0xff;
        id.data[offset + 1] = (color >> 8) & 0xff;
        id.data[offset + 2] = (color >> 0) & 0xff;
        id.data[offset + 3] = 0xff;

        this.ctx.putImageData(id, 0, 0);
    }

    protected getColor(index: number) {
        return [0x000000, 0xFFFFFF, 0x880000, 0xAAFFEE, 0xCC44CC, 0x00CC55, 0x0000AA, 0xEEEE77, 0xDD8855, 0x664400, 0xFF7777, 0x333333, 0x777777, 0xAAFF66, 0x0088FF, 0xBBBBBB][index & 0xf];
    }
}