import { Addressable } from "components/addressable";
import { Signal } from "signals";
import { Binary } from "util/binary";

enum TimerMode {
    CONTINUOUS
}

// Incomplete implementation of the 6522 versatile interface adapter chip
export class C6522 extends Addressable {
    // I/O registers
    protected ioA: number;
    protected ioB: number;

    // I/O data direction
    protected ddrA: number;
    protected ddrB: number;

    // Timer stuff, not full implemented, just what I need for now
    protected timer1Mode: TimerMode;
    protected timer1InterruptsEnabled: boolean;
    protected timer1InterruptWaiting: boolean;
    protected timer1Interval: number = 0;
    protected timer1Clocks: number = 0;

    // Signals to simulate when the via changes
    public onPortAWrite: Signal = new Signal();
    public onPortBWrite: Signal = new Signal();

    public reset() {

    }

    public clock() {
        if (
            this.timer1Mode === TimerMode.CONTINUOUS &&
            this.timer1InterruptsEnabled &&
            !this.timer1InterruptWaiting
        ) {
            this.timer1Clocks++;
            if (this.timer1Clocks >= this.timer1Interval - 2) {
                this.timer1InterruptWaiting = true;
                this.timer1Clocks = 0;
                this.cpu.irq();
            }
        }
    }

    public read() {
        const relativeAddress = this.getRelativeAddress();
        switch (relativeAddress) {
            case 0x00:
                this.cpu.data = (this.ioB & ~this.ddrB);
                break;
            case 0x01:
                this.cpu.data = (this.ioA & ~this.ddrA);
                break;
            case 0x02:
                this.cpu.data = this.ddrB;
                break;
            case 0x03:
                this.cpu.data = this.ddrA;
                break;
            case 0x04:
                // VIA t1 low-order latches / counter - reading resets interrupt
                this.timer1InterruptWaiting = false;
                this.cpu.clearInterrupt();
                break;
        }
    }

    public write() {
        const relativeAddress = this.getRelativeAddress();
        switch (relativeAddress) {
            case 0x00:
                const maskedB = this.cpu.data & this.ddrB;
                this.ioB = maskedB | (this.ioB & ~this.ddrB);
                this.onPortBWrite.dispatch();
                break;
            case 0x01:
                const maskedA = this.cpu.data & this.ddrA;
                this.ioA = maskedA | (this.ioA & ~this.ddrA);
                this.onPortAWrite.dispatch();
                break;
            case 0x02:
                this.ddrB = this.cpu.data;
                break;
            case 0x03:
                this.ddrA = this.cpu.data;
                break;
            case 0x04:
                // VIA t1 low-order latches / counter
                this.timer1Interval = (this.timer1Interval & 0xff00) + this.cpu.data;
                break;
            case 0x05:
                // VIA t1 high-order counter
                this.timer1Interval = (this.timer1Interval & 0xff) + (this.cpu.data << 8);
                break;
            case 0x0b:
                // VIA ACR
                if ((this.cpu.data >> 6) === 0b01) {
                    this.timer1Mode = TimerMode.CONTINUOUS;
                }
                break;
            case 0x0e:
                // VIA IER
                const setClear = !!(this.cpu.data & 0b10000000)
                if (this.cpu.data & 0b01000000) {
                    this.timer1InterruptsEnabled = setClear;
                }
                break;
        }
    }

    // Write to specified port, ignoring ddr output masked bits
    public portWrite(port: PortID, value: number) {
        let ddr;
        if (port === "A") {
            ddr = this.ddrA;
        } else {
            ddr = this.ddrB;
        }
        if (ddr === undefined) {
            return;
        }
        const masked = value & ~ddr;
        if (port === "A") {
            this.ioA = (this.ioA & ddr) | masked;
        } else {
            this.ioB = (this.ioB & ddr) | masked;
        }
    }

    // Read from specified port, with ddr input masked bits returned as 0
    public portRead(port: PortID) {
        if (port === "A") {
            return this.ioA & this.ddrA;
        } else {
            return this.ioB & this.ddrB;
        }
    }
}

export type PortID = "A" | "B";