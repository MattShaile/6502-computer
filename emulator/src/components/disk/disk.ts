import { C6522 } from "components/via/c6522";
import { disk } from "mount/disk";

export class Disk {

    protected marA: number;
    protected marB: number;
    protected marC: number;

    protected data: number[];

    constructor(protected via: C6522) {
        // Initialise memory with random garbage like real life
        this.data = disk;

        via.onPortAWrite.add(() => this.onControl());
    }

    protected onControl() {
        const control = this.via.portRead("A");
        const data = this.via.portRead("B");

        switch (control) {
            case 0b01000011:
                this.marA = data;
                break;
            case 0b01000101:
                this.marB = data;
                break;
            case 0b01001001:
                this.marC = data;
                break;
            case 0:
                const address = (this.marC << 16) + (this.marB << 8) + this.marA;
                this.via.portWrite("B", this.data[address]);
                break;
        }
    }
}