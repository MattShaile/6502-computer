import { Clock } from "clock/clock";
import { Controller } from "components/controller/controller";
import { C6502 } from "components/cpu/c6502";
import { SetupDebugControls } from "components/cpu/debug/c6502-debug";
import { Disk } from "components/disk/disk";
import { MattGPU } from "components/gpu/matt-gpu";
import { RAM } from "components/memory/RAM";
import { ROM } from "components/memory/ROM";
import { C6522 } from "components/via/c6522";
import { bios } from "mount/bios";

// Build a computer from components
// 65c02 CPU
const cpu = new C6502(0x8000, true);
// BIOS ROM
const rom = new ROM(cpu, 0x8000, 0xffff, 16, bios);

// RAM
const ram = new RAM(cpu, 0x0000, 0x3fff, 16);
// Versatile interface adapter
const via = new C6522(cpu, 0x6000, 0x600f, 16);

// Devices attached to VIA
// Custom GPU
const gpu = new MattGPU(via, document.getElementById("canvas") as HTMLCanvasElement);
// Custom rom disk card
const disk = new Disk(via);
// Controller card
const controller = new Controller(via);

cpu.attachDevice(rom);
cpu.attachDevice(ram);
cpu.attachDevice(via);

// Start
const customStep = () => {
    cpu.clock();
    gpu.clock();
    via.clock();
}

// Debug
SetupDebugControls(cpu, [ram, rom, via], customStep);

cpu.ready = true;

const clock = new Clock(1000000, cpu, customStep);
clock.start();

// Quick keyboard implementation
window.onkeyup = (e: any) => {
    let asciiCode = -1;
    if (e.key.length === 1) {
        asciiCode = e.key.charCodeAt(0)
    }
    if (e.key === "Enter") {
        asciiCode = 13;
    }
    if (e.key === "Backspace") {
        asciiCode = 8;
    }

    if (asciiCode > -1) {
        ram["contents"][0x206] = 1;
        ram["contents"][0x207] = asciiCode;
    }
};