const hexCache2: string[] = [];
const hexCache4: string[] = [];

export function Hex(n: number, places: number = 2, prefix = "$") {
    if (n === null) {
        return "null";
    }

    let str = places === 2 ? hexCache2[n] : hexCache4[n];

    return (prefix + str);
}
for (let n = 0; n <= 0xffff; n++) {
    let str = n.toString(16).toUpperCase();
    while (str.length < 2) {
        str = "0" + str;
    }
    if (n <= 0xff) {
        hexCache2.push(str);
    }
    while (str.length < 4) {
        str = "0" + str;
    }
    hexCache4.push(str);
}