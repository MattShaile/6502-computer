export function Binary(n: number, places: number = 8) {
    if (n === null) {
        return "null";
    }

    let str = n.toString(2);

    while (str.length < places) {
        str = "0" + str;
    }

    return "%" + str;
}