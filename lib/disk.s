; IO opcodes %0DIICCCC
; D = data direction, 1=OUT, 0=IN
; II = device select, 1-4
; CCCC = control bits
; IO device 1 (disk)
DISK_READ = %00000000

DISK_W_MARA = %01000011
DISK_W_MARB = %01000101
DISK_W_MARC = %01001001

DISK_CTRL_DIR =     %11111111
DISK_DATA_DIR_IN =  %00000000
DISK_DATA_DIR_OUT = %11111111

; loads byte from diskMarA, diskMarB, diskMarC into temp
diskRead:
  jsr disableDraw
  jsr diskWriteMode

  ; Set all 3 MARs
  lda diskMarA
  sta DATA
  lda #DISK_W_MARA
  sta CONTROL

  lda diskMarB
  sta DATA
  lda #DISK_W_MARB
  sta CONTROL

  lda diskMarC
  sta DATA
  lda #DISK_W_MARC
  sta CONTROL

  ; Set data to input
  jsr diskReadMode

  ; Read from disk
  lda #DISK_READ
  sta CONTROL

  ; Store in RAM
  lda DATA
  sta temp

  ; Reset VIA data directions to GPU mode
  jsr disableDraw
  jsr GPUMode

  rts

; Load entire 256x256 map from disk
loadMap:
  ; Select page 1
  lda #1
  sta diskMarC

  ; 256x256 loop
  ldy #$ff
  .loopy:
    dey
    ldx #$ff
    .loopx:
      dex

      jsr loadMapTile

      txa
      beq .endx
      jmp .loopx
  .endx:
    tya
    beq .endy
    jmp .loopy
  .endy:
    rts

; Load entire first 25x19 map tiles from disk
loadScreen:
  ; Select page 1
  lda #1
  sta diskMarC

  ; 25x19 loop
  ldy #19
  .loopy:
    dey
    ldx #25
    .loopx:
      dex

      jsr loadMapTile

      txa
      beq .endx
      jmp .loopx
  .endx:
    tya
    beq .endy
    jmp .loopy
  .endy:
    rts

; load tile specified by tileIndex
loadTile:
  ; Select page 0
  lda #0
  sta diskMarC

  ; Set tile address
  lda tileIndex
  sta diskMarB

  ; 16x16 loop
  ldy #16
  .loopy:
    dey
    ldx #16
    .loopx:
      dex
      ; pixel address (convert to YYYYXXXX format)
      jsr convertPixelAddress

      ; read pixel from disk
      sta diskMarA
      jsr diskRead
      lda temp
      sta color

      ; Restore pixel address
      lda diskMarA

      ; write pixel
      jsr writeTilePixel

      txa
      beq .endx
      jmp .loopx
  .endx:
    tya
    beq .endy
    jmp .loopy
  .endy:
    rts

; Load single tile specified by x and y registers from disk, store tile ID in tileIndex, and write to GPU map ram
; read tile index from disk
loadMapTile:
  sty diskMarB
  stx diskMarA
  jsr diskRead
  lda temp
  sta tileIndex

  ; write map tile
  jsr writeMapTile

  rts

; Gets the byte instructing the bios what kind of disk it's loading (and is stored in temp)
getDiskMode:
  lda #$3
  sta diskMarC
  lda #DISK_LOAD_MODE_UPPER
  sta diskMarB
  lda #DISK_LOAD_MODE_LOWER
  sta diskMarA
  jsr diskRead
  rts

; Load program from disk (page 3) and insert into RAM at address 0x300-0x3fff and set program counter to 0x300
loadProgram:
  ; Select page 3
  lda #$3
  sta diskMarC

  ; Start from PROGRAM_ENTRY
  lda PROGRAM_ENTRY_UPPER
  sta addrH
  lda PROGRAM_ENTRY_LOWER
  sta addrL

  ; Load end vector from disk to only load what's necessary
  lda #PROGRAM_END_UPPER
  sta diskMarB
  lda #PROGRAM_END_LOWER
  sta diskMarA
  jsr diskRead
  lda temp
  sta addr2L
  lda #PROGRAM_END_LOWER+1
  sta diskMarA
  jsr diskRead
  lda temp
  sta addr2H

  ; Load the current byte and store it into ram
  .checkComplete:
    ; Check for end of program
    clc
    lda addr2H
    cmp addrH
    bne .loadByte
    clc
    lda addr2L
    cmp addrL
    bne .loadByte
    jmp .start

  .loadByte:
    ; Read byte from disk
    lda addrL
    sta diskMarA
    lda addrH
    sta diskMarB
    jsr diskRead
    ; Set up address in zero page for indirect indexing
    lda addrL
    sta $0
    lda addrH
    sta $1
    ldx #0
    lda temp
    sta ($0,X)
    ; Move to next byte
    inc addrL
    bne .checkComplete
    inc addrH
    jmp .checkComplete

  .start:
    ; Start executing from program loaded into RAM from disk
    jmp PROGRAM_ENTRY

; Setup via for reading from disk
diskReadMode:
  ; Set all pins on port A (DATA) to input
  lda #DISK_DATA_DIR_IN
  sta DATA_DIR

  rts

; Setup via for writing memory address to disk
diskWriteMode:
  ; Set all pins on port A (DATA) to output
  lda #DISK_DATA_DIR_OUT
  sta DATA_DIR
  ; Set all pins on port B (CONTROL) to output, except pin 4 and 5 (hblank and vblank)
  lda #DISK_CTRL_DIR
  sta CTRL_DIR

  rts

; Load all 256 tiles from the disk (slow way)
loadTiles:
  lda #$ff
  sta tileIndex
  .tileIndexLoop:
    dec tileIndex
    jsr loadTile
    lda tileIndex
    bne .tileIndexLoop
  rts

; Load all 256 tiles from the disk
dumpTiles:
  ; Select page 0 on disk (tiles)
  jsr diskWriteMode

  lda #0
  sta diskMarC

  ; Use y and x for upper and lower bytes to loop through whole page
  ldx #0
  ldy #0
  .loopy:
    ; Transfer y to diskMarB
    sty diskMarB
    .loopx:
      ; Transfer x to diskMarA
      stx diskMarA

      ; Load from disk
      jsr diskRead

      ; Store in VRAM
      jsr GPUMode
      ; address
      stx DATA
      lda #MARA
      sta CONTROL
      sty DATA
      lda #MARB
      sta CONTROL

      ; write
      lda temp
      sta DATA
      lda #TILE
      sta CONTROL
      jsr disableDraw

      ; Loop until all done
      inx
      txa
      bne .loopx
    iny
    tya
    bne .loopy

  ; Reset VIA data directions to GPU mode
  jsr disableDraw
  jsr GPUMode

  rts