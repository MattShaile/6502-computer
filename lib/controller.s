; IO opcodes %0DIICCCC
; D = data direction, 1=OUT, 0=IN
; II = device select, 1-4
; CCCC = control bits
; IO device 2 (controller)
CONTROLLER_READ = %00010000

CONTROLLER_CTRL_DIR =     %11111111
CONTROLLER_DATA_DIR_IN =  %00000000

; load controller input into a
readController:
    ; Set VIA data directions
    lda #CONTROLLER_DATA_DIR_IN
    sta DATA_DIR
    lda #CONTROLLER_CTRL_DIR
    sta CTRL_DIR

    ; Read from controller IO
    lda #CONTROLLER_READ
    sta CONTROL
    lda DATA

    rts