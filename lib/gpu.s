; GPU opcodes
ENABLE_DRAW =    %10000000
DISABLE_DRAW =   %11000000

MARA =           %11000001
MARB =           %11000011
MAP =            %11000101
TILE =           %11000111
SXA =            %11001001
SXB =            %11001011
SYA =            %11001101
SYB =            %11001111

VBLANK =         %00100000

GPU_CTRL_DIR =   %11001111


; Set tile at tileIndex to a solid color, stored in color
solidTile:
  ; 16x16 loop
  ldy #16
  .loopy:
    dey
    ldx #16
    .loopx:
      dex

      ; pixel address
      jsr convertPixelAddress
      jsr writeTilePixel

      txa
      beq .endloopy
      jmp .loopx
  .endloopy:
    tya
    beq .endloopx
    jmp .loopy
  .endloopx:
    rts

; Draw a 16x16 grid of all available tiles
drawAllTiles:

  lda #$ff
  sta tileIndex

  ldy #16
  .loopy:
    dey
    ldx #16
    .loopx:
      dex

      jsr writeMapTile

      ; next tile
      txa
      ldx tileIndex
      dex
      stx tileIndex
      tax

      beq .endloopy
      jmp .loopx
  .endloopy:
    tya
    beq .endloopx
    jmp .loopy
  .endloopx:
    rts

; Fill map with tileIndex
fillMap:
  ldy #255
  .loopy:
    dey
    ldx #255
    .loopx:
      dex

      jsr writeMapTile

      txa
      beq .endloopy
      jmp .loopx
  .endloopy:
    tya
    beq .endloopx
    jmp .loopy
  .endloopx:
    rts

; Write color stored in color to tile id in tileIndex to pixel address stored in a
writeTilePixel:
  ; address
  sta DATA
  lda #MARA
  sta CONTROL
  lda tileIndex
  sta DATA
  lda #MARB
  sta CONTROL

  ; write
  lda color
  sta DATA
  lda #TILE
  sta CONTROL
  jsr disableDraw
  rts

; Set map tile at x/y register to tile index stored in tileIndex
writeMapTile:
  ; address
  stx DATA
  lda #MARA
  sta CONTROL
  sty DATA
  lda #MARB
  sta CONTROL

  ; write
  lda tileIndex
  sta DATA
  lda #MAP
  sta CONTROL
  jsr disableDraw
  rts

updateScroll:
  lda scrollXL
  sta DATA
  lda #SXA
  sta CONTROL

  lda scrollXH
  sta DATA
  lda #SXB
  sta CONTROL
  
  lda scrollYL
  sta DATA
  lda #SYA
  sta CONTROL
  
  lda scrollYH
  sta DATA
  lda #SYB
  sta CONTROL

  rts

enableDraw:
  lda #ENABLE_DRAW
  sta CONTROL
  rts

  
disableDraw:
  lda #DISABLE_DRAW
  sta CONTROL
  rts

waitForVBlank:
  .loop:
    lda CONTROL
    and #VBLANK
    beq .loop
    rts

waitForVDraw:
  .loop:
    lda CONTROL
    and #VBLANK
    bne .loop
    rts

GPUMode:
  ; Set all pins on port A (DATA) to output
  lda #%11111111 
  sta DATA_DIR
  ; Set all pins on port B (CONTROL) to output, except pin 4 and 5 (hblank and vblank)
  lda #GPU_CTRL_DIR
  sta CTRL_DIR

  rts

; Converts x and y reg into single 8 bit pixel address and stores in a reg
convertPixelAddress:
  stx temp
  tya
  .repeat 4
  clc
  asl
  .endrepeat
  ora temp
  rts