; ; OSI Defines
CURPOS_X   = $200        ; ROM BASIC cursor position
CURPOS_Y  = $201        ; ROM BASIC cursor position
DEBUG_CHAR = $202
LOADFLAG = $0203        ; ROM BASIC LOAD flag
SAVEFLAG = $0205        ; ROM BASIC SAVE flag

KEYBOARD_PENDING      = $206        ; OSI polled keyboard register
KEYBOARD_CHAR      = $207        ; OSI polled keyboard register

SAVE_X = $DE		; For saving registers
SAVE_Y = $DF

; put the IRQ and NMI code in RAM so that it can be changed

IRQ_vec	= VEC_SV+2	; IRQ code vector
NMI_vec	= IRQ_vec+$0A	; NMI code vector

; reset vector points here

RES_vec
	CLD				; clear decimal mode
	LDX	#$FF			; empty stack
	TXS				; set the stack
	ldx #0
	stx CURPOS_X
	stx CURPOS_Y
	jmp LAB_COLD

BASIC_OUTPUT
	STX     SAVE_X                  ; Preserve X register
	STY     SAVE_Y                  ; Preserve Y register
	sta DEBUG_CHAR
	sta tileIndex

	; Check for back space
	lda #8
	clc
	cmp DEBUG_CHAR
	beq .backSpace

	; Check for line feed
	lda #10
	clc
	cmp DEBUG_CHAR
	beq .lineFeed

	; Check for carriage return
	lda #13
	clc
	cmp DEBUG_CHAR
	beq .carriageReturn

	jmp .drawChar

	.backSpace:
		ldx CURPOS_X
		dex
		stx CURPOS_X
		lda #32
		sta tileIndex

	.drawChar:
		; Draw char at cursor pos
		ldx CURPOS_X
		ldy CURPOS_Y
		jsr writeMapTile

		; Check for back space again
		lda #8
		clc
		cmp DEBUG_CHAR
		beq .end
		; Move cursor (with wrapping)
		inx
		stx CURPOS_X
		jmp .end

	.carriageReturn:
		ldy CURPOS_Y
		iny
		sty CURPOS_Y
		; Check for scroll
		lda #17
		clc
		cmp CURPOS_Y
		bcs .end
		; Just return to top of screen
		ldy #0
		sty CURPOS_Y
		; Scroll the screen
		; lda #16
		; clc
		; adc scrollYL
		; sta scrollYL
		; bne .updateScroll
		; ; Scroll high order byte
		; inc scrollYH
		; .updateScroll:
		; 	jsr updateScroll
			jmp .end

	.lineFeed:
		; Reset CURPOS_X
		ldx #0
		stx CURPOS_X
		
		; Clear line
		stx tileIndex
		ldy CURPOS_Y
		ldx #25
		.clearNextChar:
			dex
			jsr writeMapTile
			txa
			bne .clearNextChar
		jmp .end

	.end:
	; Renable drawing, restore x, y and a
		jsr enableDraw
		lda DEBUG_CHAR
		ldx     SAVE_X
		ldy     SAVE_Y
	rts

; byte in from keyboard

BASIC_INPUT
	stx     SAVE_X                  ; Preserve X register
	sty     SAVE_Y                  ; Preserve Y register

	clc
	lda #0
	cmp KEYBOARD_PENDING
	beq .noKey

	; Key is pressed
	lda #0
	sta KEYBOARD_PENDING
	lda KEYBOARD_CHAR
	sec
	jmp .end
	.noKey:
		clc
	.end:
		ldx SAVE_X
		ldy SAVE_Y
	rts

BASIC_LOAD				        ; load vector for EhBASIC
        LDA     #$80                    ; Set OSI ROM LOAD flag
        STA     LOADFLAG
	RTS

BASIC_SAVE				        ; save vector for EhBASIC
        LDA     #$01                    ; Set OSI ROM SAVE flag
        STA     SAVEFLAG
	RTS

; vector tables

; LAB_vec
; 	.word	KBDin                   ; byte in from keyboard
; 	.word	SCRNout		        ; byte out to screen
; 	.word	OSIload		        ; load vector for EhBASIC
; 	.word	OSIsave		        ; save vector for EhBASIC

; EhBASIC IRQ support

; IRQ_CODE
; 	PHA				; save A
; 	LDA	IrqBase		        ; get the IRQ flag byte
; 	LSR				; shift the set b7 to b6, and on down ...
; 	ORA	IrqBase		        ; OR the original back in
; 	STA	IrqBase		        ; save the new IRQ flag byte
; 	PLA				; restore A
; 	RTI

; ; EhBASIC NMI support

; NMI_CODE
; 	PHA				; save A
; 	LDA	NmiBase		        ; get the NMI flag byte
; 	LSR				; shift the set b7 to b6, and on down ...
; 	ORA	NmiBase		        ; OR the original back in
; 	STA	NmiBase		        ; save the new NMI flag byte
; 	PLA				; restore A
; 	RTI

END_CODE

; LAB_mess
; 	.byte	$0D,$0A,"6502 EhBASIC",$0D,$0A, "[C]old/[W]arm?",$00
; 					; sign on string