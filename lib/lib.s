  .include variables.s
  .include reset.s
  .include interrupts.s
  .include gpu.s
  .include disk.s
  .include controller.s
  .include utils.s