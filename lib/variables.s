; List all reserved ram addresses to avoid conflicts

; Hard wired to VIA
DATA =              $6000 ; VIA port B
CONTROL =           $6001 ; VIA port A
DATA_DIR =          $6002 ; VIA port B ddr
CTRL_DIR =          $6003 ; VIA port A ddr
VIA_T1CL =          $6004 ; VIA t1 low-order latches / counter
VIA_T1CH =          $6005 ; VIA t1 high-order counter
VIA_T1LL =          $6006 ; VIA t1 low-order latches
VIA_T1LH =          $6007 ; VIA t1 high-order latches
VIA_T2CL =          $6008 ; VIA t2 low-order latches / counter
VIA_T2CH =          $6009 ; VIA t2 high-order counter
VIA_SR =            $600a ; VIA Shift register
VIA_ACR =           $600b ; VIA Auxillary control register
VIA_PCR =           $600c ; VIA Peripheral control register
VIA_IFR =           $600d ; VIA Interrupt flag register
VIA_IER =           $600e ; VIA Interrupt enable register
VIA_ORA_IRA =       $600f ; VIA port A with no handshake

; Epic6502 conventional addresses
INTERRUPT_ADDRESS = $3fe0 ; Interrupt handler start

; Epic6502 conventional disk entry point
PROGRAM_ENTRY = $0403
PROGRAM_ENTRY_UPPER = $04
PROGRAM_ENTRY_LOWER = $03
; Location of word on disk for address of program end
PROGRAM_END = $0401
PROGRAM_END_UPPER = $04
PROGRAM_END_LOWER = $01
; Location of byte on disk for disk mode
DISK_LOAD_MODE = $0400
DISK_LOAD_MODE_UPPER = $04
DISK_LOAD_MODE_LOWER = $00

; General purpose variables
temp =              $3f00 ; General purpose for functions to pass input / output
ticksA =            $3f01 ; Lowest order byte of 4 byte "ticks" - which is how many times 50,000 clock (10ms on 1Mhz clock) timer has been called
ticksB =            $3f02
ticksC =            $3f03
ticksD =            $3f04
addrH =             $3f05 ; General purpose for functions to store a 2 byte address
addrL =             $3f06 ; General purpose for functions to store a 2 byte address
addr2H =            $3f07 ; General purpose for functions to store a 2 byte address
addr2L =            $3f08 ; General purpose for functions to store a 2 byte address

; Variables for GPU
tileIndex =         $3f10 ; General use to specify a tileIndex for various functions
color =             $3f11 ; General use to specify a color for various functions
scrollXL =          $3f12 ; Lower bytes of X scroll offset
scrollXH =          $3f13 ; Upper bytes of X scroll offset
scrollYL =          $3f14 ; Lower bytes of Y scroll offset
scrollYH =          $3f15 ; Upper bytes of Y scroll offset

; Variables for disk expansion card
diskMarA =          $3f20 ; MARA for disk address
diskMarB =          $3f21 ; MARB for disk address
diskMarC =          $3f22 ; MARC for disk address