reset:
  ; ==== Load default interrupt handler into ram (just an rti) - will be overriden by disk interrupt
  lda #$60
  sta INTERRUPT_ADDRESS

  ; ===== CPU =====

  ; Reset stack pointer
  ldx #$ff 
  txs

  jsr GPUMode

  lda #0
  sta DATA

  ; Reset X and Y registers
  ldx #0
  ldy #0

  ; Disable interrupts & setup timers
  sei
  jsr setupTimers

  ; ===== GPU =====

  ; Reset VRAM MARs
  lda #MARA 
  sta CONTROL
  lda #MARB
  sta CONTROL

  ; Reset scroll registers  
  lda #0
  sta scrollXL
  sta scrollXH
  sta scrollYL
  sta scrollYH

  jsr updateScroll

  ; ===== Entry point =====

  ; Init hook
  jmp init