irq:
    ; Handle bios default interrupts, such as timers
    bit VIA_T1CL
    inc ticksA
    bne .increaseTicksEnd
    inc ticksB
    bne .increaseTicksEnd
    inc ticksC
    bne .increaseTicksEnd
    inc ticksD
    .increaseTicksEnd:
    sei

    ; Jump to 300, which is the disk's interrupt handler
    jsr INTERRUPT_ADDRESS
    rti

setupTimers:
    ; Reset ticks
    lda #0
    sta ticksA
    sta ticksB
    sta ticksC
    sta ticksD
    ; Set VIA timer 1 to continuous interrupt mode
    lda #%01000000
    sta VIA_ACR
    ; Set timer to trigger every 50,000 cycles (50ms with 1mhz clock)
    lda #$0e
    sta VIA_T1CL
    lda #$27
    sta VIA_T1CH
    lda #%11000000
    sta VIA_IER
    rts