const utils = require("./utils");
const fs = require("fs");
const exec = require('child_process').execFile;

if (process.argv.length < 3) {
    console.log("No bios name specified. Please specify a bios name eg. `npm run compile-bios -- myBios`");
    process.exit(0);
}

const name = process.argv[2];
const inputPath = `./bios/${name}`;
const exists = fs.existsSync(inputPath)

if (!exists) {
    console.log(`Bios with name: "${name}" not found`);
    process.exit(0);
}

console.log(`Creating bios: ${name}`);

const outputPath = `${inputPath}/${name}.bin`;

exec(`./compiler/vasm6502_oldstyle.exe`, ["-Fbin", "-dotdir", "-o", outputPath, "-I", "../../lib", `${inputPath}/main.s`], function callback(error, data) {
    if (error) {
        console.log(error);
        process.exit(0);
    } else {
        console.log("Compilation successful");

        const bytes = [];
        fs.stat(outputPath, function (err, stats) {
            const fileSize = stats.size;
            const bytesToRead = fileSize;
            const position = fileSize - bytesToRead;
            fs.open(outputPath, 'r', function (errOpen, fd) {
                fs.read(fd, Buffer.alloc(bytesToRead), 0, bytesToRead, position, function (errRead, bytesRead, buffer) {
                    for (const byte of buffer.values()) {
                        bytes.push(byte);
                    }

                    console.log("Mounting to emulator");

                    fs.writeFileSync("./emulator/src/mount/bios.ts", `export const bios:number[] = [${bytes}];`);

                    console.log("Done");
                });
            });

        });
    }
});