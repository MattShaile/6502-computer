const utils = require("./utils");
const fs = require("fs");
const exec = require('child_process').execFile;
const libpng = require("node-libpng");

async function compileDisk() {
    if (process.argv.length < 3) {
        console.log("No disk name specified. Please specify a disk name eg. `npm run compile-disk -- myDisk`");
        process.exit(0);
    }

    const name = process.argv[2];
    const inputPath = `./disk/${name}`;
    const exists = fs.existsSync(inputPath)

    if (!exists) {
        console.log(`Disk with name: "${name}" not found`);
        process.exit(0);
    }

    console.log(`Creating disk: ${name}`);

    const outputPath = `${inputPath}/${name}.bin`;
    const programOutputPath = `${inputPath}/program.bin`;

    // Read tiles from image and reformat for GPU compatibility
    const pixelData = [];
    const tileData = libpng.readPngFileSync(`${inputPath}/tiles.png`);
    const tilesBuffer = tileData.data;
    for (const tilePixel of tilesBuffer.values()) {
        pixelData.push(tilePixel);
    }
    if (tileData.width !== 256 || tileData.height !== 256) {
        console.log("Invalid tileset image size, must be 256x256 pixels");
        process.exit(0);
    }

    const tiles = [];
    for (let tiley = 0; tiley < 16; tiley++) {
        for (let tilex = 0; tilex < 16; tilex++) {
            for (let y = 0; y < 16; y++) {
                for (let x = 0; x < 16; x++) {
                    const pixel = pixelData[(x + tilex * 16) + ((y + tiley * 16) * 256)];
                    tiles.push(pixel);
                }
            }
        }
    }

    // Read tilemap from Tiler tmx file
    const mapBuffer = fs.readFileSync(`${inputPath}/map.tmx`);
    const mapData = mapBuffer.toString();
    const dataStart = `<data encoding="csv">`;
    // Parse data out of tmx file; tile index is +1 in tiler due to 0 being reserved for transparency, so it should be reduced by 1 if possible
    const tilemap = mapData.slice(mapData.indexOf(dataStart) + dataStart.length + 1, mapData.indexOf("</data>")).split(",").map((tile) => Math.max(tile - 1, 0));
    if (tilemap.length !== 256 * 256) {
        console.log(tilemap.length);
        console.log("Invalid map size, must be 256x256 tiles");
    }

    // Load program
    console.log("Compiling disk program");

    exec(`./compiler/vasm6502_oldstyle.exe`, ["-Fbin", "-dotdir", "-o", programOutputPath, "-I", "../../lib", `${inputPath}/main.s`], function callback(error, data) {
        if (error) {
            console.log(error);
            process.exit(0);
        } else {
            console.log("Compilation successful");

            const program = new Array(0x400).fill(0);
            fs.stat(programOutputPath, function (err, stats) {
                const fileSize = stats.size;
                const bytesToRead = fileSize;
                const position = fileSize - bytesToRead;
                fs.open(programOutputPath, 'r', function (errOpen, fd) {
                    fs.read(fd, Buffer.alloc(bytesToRead), 0, bytesToRead, position, function (errRead, bytesRead, buffer) {
                        for (const byte of buffer.values()) {
                            program.push(byte);
                        }

                        // Create binary
                        // 0x00000-0x0ffff = tileset
                        // 0x10000-0x1ffff = map
                        // 0x20000-0x2ffff = tileset 2
                        // 0x30000-0x33999 = program
                        let disk = tiles.concat(tilemap).concat(tiles).concat(program);
                        if (disk.length < 0x3ffff) {
                            disk = disk.concat(new Array(0x3ffff - disk.length).fill(0));
                        }

                        const binary = new Buffer.from(disk);

                        console.log("Writing binary output");

                        fs.writeFileSync(outputPath, binary);

                        console.log("Binary output created");

                        console.log("Mounting to emulator");

                        fs.writeFileSync("./emulator/src/mount/disk.ts", `export const disk:number[] = [${disk}];`);

                        console.log("Done");
                    });
                });

            });
        }
    });
}

compileDisk();