function formatBinary(bytes) {
    let formatted = "\n";
    let chunk = [];
    let lineChunkCount = 0;
    bytes.forEach((byte) => {
        byte = byte.toString(16);
        if (byte.length < 2) {
            byte = "0" + byte;
        }
        chunk.push(byte);
        if (chunk.length === 8) {
            formatted += chunk.join(" ");
            chunk = [];

            lineChunkCount++;
            if (lineChunkCount === 1) {
                formatted += "  ";
            } else if (lineChunkCount === 2) {
                lineChunkCount = 0;
                formatted += "\n";
            }
        }
    });

    return formatted;
}

exports.formatBinary = formatBinary;