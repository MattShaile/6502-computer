  .org $8000

  .include ../../lib/lib.s
  
  .org INTERRUPT_ADDRESS
  rts

  .org PROGRAM_ENTRY

init:
loop:
  jsr drawTimer

  .draw:
  jsr enableDraw
  jsr waitForVDraw
  jsr waitForVBlank
  jsr updateScroll
  jmp loop

drawTimer:
  lda ticksA
  and #%00001111
  tax
  inx
  txa
  sta tileIndex
  ldx #16
  ldy #10
  jsr writeMapTile

  lda ticksA
  and #%11110000
  .repeat 4
  lsr
  .endrepeat
  tax
  inx
  txa
  sta tileIndex
  ldx #15
  ldy #10
  jsr writeMapTile

  lda ticksB
  and #%00001111
  tax
  inx
  txa
  sta tileIndex
  ldx #14
  ldy #10
  jsr writeMapTile

  lda ticksB
  and #%11110000
  .repeat 4
  lsr
  .endrepeat
  tax
  inx
  txa
  sta tileIndex
  ldx #13
  ldy #10
  jsr writeMapTile

  lda ticksC
  and #%00001111
  tax
  inx
  txa
  sta tileIndex
  ldx #12
  ldy #10
  jsr writeMapTile

  lda ticksC
  and #%11110000
  .repeat 4
  lsr
  .endrepeat
  tax
  inx
  txa
  sta tileIndex
  ldx #11
  ldy #10
  jsr writeMapTile
  rts