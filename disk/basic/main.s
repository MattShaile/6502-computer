  .org $8000

  .include ../../lib/lib.s
  
  .org INTERRUPT_ADDRESS
  rts

  .org DISK_LOAD_MODE
  .byte 1

  .org PROGRAM_ENTRY
init:
  sei
  jmp	$C000

programEnd:
  .org PROGRAM_END
  .word programEnd