  .org $8000

  .include ../../lib/lib.s
  
  .org INTERRUPT_ADDRESS
  rts

  .org DISK_LOAD_MODE
  .byte 1

  .org PROGRAM_ENTRY
init:
  sei
  loop:
    ldx #147
    jsr readController
    cmp #%00000010
    beq .drawbtn
    ldx #35
    .drawbtn:
      stx tileIndex
      ldx #9
      ldy #8
      jsr GPUMode
      jsr writeMapTile
    jmp	loop

programEnd:
  .org PROGRAM_END
  .word programEnd