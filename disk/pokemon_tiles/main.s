  .org $8000

  .include ../../lib/lib.s
  
  .org INTERRUPT_ADDRESS
  rts

  .org PROGRAM_ENTRY

init:
loop:

  ; Scroll Y
  .scrollY:
  inc scrollYL
  bne .checkEndOfScreen
  inc scrollYH

  ; Check to see if scroll Y has reached 1744 (bottom of map visible)
  .checkEndOfScreen:
  lda #%00000110
  clc
  cmp scrollYH
  bne .draw
  lda #%11010000
  clc
  cmp scrollYL
  bne .draw

  ; Reset Y scroll
  lda #0
  sta scrollYH
  sta scrollYL
  
  ; Check if x is already scrolled
  lda scrollXH
  bne .resetXScroll

  ; Set X scroll to 400
  lda #%00000001
  sta scrollXH
  lda #%10010000
  sta scrollXL
  jmp .draw
  .resetXScroll:
  lda #0
  sta scrollXH
  sta scrollXL

  .draw:
  jsr enableDraw
  jsr waitForVDraw
  jsr waitForVBlank
  jsr updateScroll
  jmp loop