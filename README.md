
# EPIC6502

  

I will write a better overview here eventually :)

  

This repository emulates Matthew Shaile's homebrew 6502 computer (identical schematic to the 6502 computer project by [BenEater](https://eater.net/)) and custom graphics card.

It also facilitates creation of BIOS and Disk images specific to my design

  

## 📁 Folders

  

-  `emulator` A browser-based emulator of the computer (including a 65c02 CPU emulation) written in Typescript

-  `lib` Assembly library of convenience functions used by the bios - can also be accessed by programs read from disk

-  `bios` BIOS assembly sources

-  `disk` 2Mb flash ROM sources and binaries

-  `compiler` vasm assembler (http://sun.hasenbraten.de/vasm/)

-  `docs` Documentation, gerber files, BOM etc (to be added)

-  `photos` Photos of the computer (to be added)

-  `resources` Color palette and examples of source files used to create disks

-  `scripts` Build scripts used for the various npm commands available for use in this repository

  

## 🛠️ Setting up

  

Run `npm i` from the project root

  

## 🖥️ Emulator

  

### Running

  

Run `npm run emulate` (or `npm start`) from the project root and open a browser to `http://localhost:6502`

  

### Mounting bios / disk

Once a valid bios and disk image has been created, they will be placed in the `emulator/src/mount` folder, which the emulator will use

  

## 📀 BIOS

  

### Compiling

Write source code (6502 assembly) in `bios/[name]/main.s`, then run `npm compile-bios [name]` to compile the bios. An emulator-compatible typescript export will automatically be placed in `./emulator/src/mount/bios.ts` and a `[name].bin` will be placed in your bios' source directory (`bios/[name]/[name.bin]`)

  

## 💾 Disk

  

### Creating maps

  

To create a map use "Tiled" (https://www.mapeditor.org/) and create a 256x256 map of 16x16 tiles

  

### Creating tilesets

  

Use photoshop to create tiles in a 16x16 grid of 16x16 pixel tiles (total image size 256x256) and set the color mode to indexed color, using the `.act` file provided in the `resources` folder

  

### Creating programs

  

Programs are loaded from disk by the bios, their execution starts from $403. The BIOS will replace the contents of RAM with this program, therefore the maximum program size is $3999 bytes. The program counter will start at $403 (`PROGRAM_ENTRY`).

  

Note that the BIOS libraries can still be used - use this boilerplate to write programs which are loaded from disk:

  

```
  .org $8000

  .include ../../lib/lib.s
  
  .org INTERRUPT_ADDRESS
  rts

  .org DISK_LOAD_MODE
  .byte 1

  .org PROGRAM_ENTRY
init:
  sei
  jmp	$C000

programEnd:
  .org PROGRAM_END
  .word programEnd

```

  

### Compiling disks

Place tileset image, map data, and source code in `disk/[name]` folder with the following names:

-  `tiles.png`

-  `map.tmx`

-  `main.s`

Run `npm run compile-disk [name]` to compile. The resulting valid disk binary will be written to your source directory (`disk/[name]/[name].bin`) and an emulator-compatible typescript export will automatically be placed in `./emulator/src/mount/disk.ts`. Note a program.bin file will also be created, but this isn't useful on it's own as it needs to be inserted into the correct memory location by the bios

### Example programs

- basic: Enhanced BASIC
- controller: Test for game controller card
- pokemon_tiles: Nice graphics example
- time: Displays a counter